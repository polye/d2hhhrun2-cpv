from Configurables import DaVinci
from Configurables import DecayTreeTuple
import DecayTreeTuple.Configuration

year = DaVinci().DataType
channel = DaVinci().TupleFile.split('.')[0]
print channel
print year   

mother = 'D'
motherDTF = 'D'
if (channel == 'D2PPK' ) : 
  motherDTF= 'D_DTF'

if (channel == 'Ds2KKP' or channel == 'Ds2PPP' or channel == 'Ds2KKK' or channel == 'Ds2KPP' ) : 
   mother = 'Ds'
   motherDTF= 'Ds'

if (channel == 'Ds2PPK' ) : 
   mother = 'Ds'
   motherDTF= 'Ds_DTF1'
if (channel == 'Ds2PKK'  ) : 
   mother = 'Ds'
   motherDTF= 'Ds_DTF2'
print mother 
print motherDTF

rootInTES = '/Event/Turbo'  #2015  and #2016 
if (year == '2017' or year == '2018' ):
     rootInTES =  '/Event/Charmcharged/Turbo'
     if (channel == 'D2KPP' or channel == 'Ds2KKP') : rootInTES = '/Event/Charmspec/Turbo'
   
Dhhh_line = ''
Dhhh_decay = ''
Dhhh_branches = ''
Dhhh_Tuplename = ''
Dhhh_Daug1 = ''

if (channel == 'D2KPP'):
   Dhhh_Tuplename = "ntp_KPP"
   Dhhh_line = 'Hlt2CharmHadDpToKmPipPipTurbo'
   Dhhh_decay = "[D+ -> ^K- ^pi+ ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D+ -> K- pi+ pi+)]CC"
    ,  "p1" :  "[D+ -> ^K- pi+ pi+]CC "
    ,  "p2" :  "[D+ ->  K- ^pi+ pi+]CC "
    ,  "p3" :  "[D+ -> K- pi+ ^pi+]CC"
   }
   Dhhh_Daug1 = 'Km'

if (channel == 'D2KKP'):
   Dhhh_Tuplename = "ntp_KKP"
   Dhhh_line = 'Hlt2CharmHadDpToKmKpPipTurbo'
   Dhhh_decay = "[D+ -> ^K- ^K+ ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D+ -> K- K+ pi+)]CC"
      ,  "p1" :  "[D+ -> ^K- K+ pi+]CC "
      ,  "p2" :  "[D+ ->  K- ^K+ pi+]CC "
      ,  "p3" :  "[D+ -> K- K+ ^pi+]CC"
   }
   Dhhh_Daug1 = 'Km'


if (channel == 'D2PPP'):
   Dhhh_Tuplename = "ntp_PPP"
   Dhhh_line = 'Hlt2CharmHadDpToPimPipPipTurbo'
   Dhhh_decay = "[D+ -> ^pi- ^pi+ ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D+ -> pi- pi+ pi+)]CC"
     ,  "p1" :  "[D+ ->  ^pi- pi+ pi+]CC "
     ,  "p2" :  "[D+ ->pi- ^pi+ pi+]CC"
     ,  "p3" :  "[D+ -> pi- pi+ ^pi+]CC "
   }
   Dhhh_Daug1 = 'Pim'


if (channel == 'D2PPK'):
   Dhhh_Tuplename = "ntp_KPPos"
   Dhhh_line = 'Hlt2CharmHadDpToKpPimPipTurbo'
   Dhhh_decay = "[D+ -> ^K+ ^pi- ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D+ -> K+ pi- pi+)]CC"
     ,  "p1" :  "[D+ -> K+  ^pi-  pi+ ]CC "
     ,  "p2" :  "[D+ -> K+  pi-  ^pi+ ]CC "
     ,  "p3" :  "[D+ -> ^K+  pi-  pi+ ]CC "
   }
   Dhhh_Daug1 = 'Pim'

if (channel == 'D2KKK'):
   Dhhh_Tuplename = "ntp_KKK"
   Dhhh_line = 'Hlt2CharmHadDpToKmKpKpTurbo'
   Dhhh_decay = "[D+ -> ^K- ^K+ ^K+]CC"
   Dhhh_branches = {
    "D" :  "[^(D+ -> K- K+ K+)]CC"
      ,  "p1" :  "[D+ ->^K-  K+  K+ ]CC "
      ,  "p2" :  "[D+ -> K- ^K+ K+ ]CC "
      ,  "p3" :  "[D+ ->K- K+  ^K+ ]CC"
   }
   Dhhh_Daug1 = 'Km'


if (channel == 'Ds2PKK'):
   Dhhh_Tuplename = "ntp_KKPos"
   Dhhh_line = 'Hlt2CharmHadDspToKpKpPimTurbo'
   Dhhh_decay = "[D_s+ -> ^K+ ^K+ ^pi-]CC"
   Dhhh_branches = {
   "D" :  "[^(D_s+ -> K+ K+ pi-)]CC"
    ,  "p1" :  "[D_s+ -> K+  K+  ^pi- ]CC "
    ,  "p2" :  "[D_s+ -> K+  ^K+  pi- ]CC "
    ,  "p3" :  "[D_s+ -> ^K+  K+  pi- ]CC "
   }
   Dhhh_Daug1 = 'Pim'

if (channel == 'Ds2KKP'):
   Dhhh_Tuplename = "ntp_DsKKP"
   Dhhh_line = 'Hlt2CharmHadDspToKmKpPipTurbo'
   Dhhh_decay = "[D_s+ -> ^K- ^K+ ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D_s+ -> K- K+ pi+)]CC"
      ,  "p1" :  "[D_s+ -> ^K- K+ pi+]CC "
      ,  "p2" :  "[D_s+ ->  K- ^K+ pi+]CC "
      ,  "p3" :  "[D_s+ -> K- K+ ^pi+]CC"
   }
   Dhhh_Daug1 = 'Km'

if (channel == 'Ds2PPP'):
   Dhhh_Tuplename = "ntp_DsPPP"
   Dhhh_line = 'Hlt2CharmHadDspToPimPipPipTurbo'
   Dhhh_decay = "[D_s+ -> ^pi- ^pi+ ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D_s+ -> pi- pi+ pi+)]CC"
      ,  "p1" :  "[D_s+ -> ^pi- pi+ pi+]CC "
      ,  "p2" :  "[D_s+ ->  pi- ^pi+ pi+]CC "
      ,  "p3" :  "[D_s+ -> pi- pi+ ^pi+]CC"
   }
   Dhhh_Daug1 = 'Pim'

if (channel == 'Ds2PPK'):
   Dhhh_Tuplename = "ntp_DsKPPos"
   Dhhh_line = 'Hlt2CharmHadDspToKpPimPipTurbo'
   Dhhh_decay = "[D_s+ -> ^K+ ^pi- ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D_s+ -> K+ pi- pi+)]CC"
     ,  "p1" :  "[D_s+ -> K+ ^pi- pi+ ]CC "
     ,  "p2" :  "[D_s+ -> K+ pi- ^pi+ ]CC "
     ,  "p3" :  "[D_s+ -> ^K+ pi- pi+ ]CC "
   }
   Dhhh_Daug1 = 'Pim'

if (channel == 'Ds2KKK'):
   Dhhh_Tuplename = "ntp_DsKKK"
   Dhhh_line = 'Hlt2CharmHadDspToKmKpKpTurbo'
   Dhhh_decay = "[D_s+ -> ^K- ^K+ ^K+]CC"
   Dhhh_branches = {
    "D" :  "[^(D_s+ -> K- K+ K+)]CC"
      ,  "p1" :  "[D_s+ -> ^K- K+ K+]CC "
      ,  "p2" :  "[D_s+ ->  K- ^K+ K+]CC "
      ,  "p3" :  "[D_s+ -> K- K+ ^K+]CC"
   }
   Dhhh_Daug1 = 'Km'

if (channel == 'Ds2KPP'):
   Dhhh_Tuplename = "ntp_DsKPP"
   Dhhh_line = 'Hlt2CharmHadDspToKmPipPipTurbo'
   Dhhh_decay = "[D_s+ -> ^K- ^pi+ ^pi+]CC"
   Dhhh_branches = {
    "D" :  "[^(D_s+ -> K- pi+ pi+)]CC"
    ,  "p1" :  "[D_s+ -> ^K- pi+ pi+]CC "
    ,  "p2" :  "[D_s+ ->  K- ^pi+ pi+]CC "
    ,  "p3" :  "[D_s+ -> K- pi+ ^pi+]CC"
   }
   Dhhh_Daug1 = 'Km'


from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *


from Configurables import  TupleToolTISTOS,L0TriggerTisTos, TriggerTisTos, TupleToolPrimaries

from Configurables import GaudiSequencer, ChargedProtoANNPIDConf
from Configurables import  LoKi__Hybrid__TupleTool


tmpToolList = [ "TupleToolPrimaries", "TupleToolEventInfo", "TupleToolRecoStats", "TupleToolL0Data"]
daugToolList = [ "TupleToolKinematic" ,"TupleToolPid","TupleToolGeometry",   "LoKi::Hybrid::TupleTool/LoKi_Dau" ]

motherToolList = [ "TupleToolKinematic", "TupleToolPid","TupleToolGeometry","TupleToolPropertime",  "LoKi::Hybrid::TupleTool/LoKi_Mother","TupleToolTISTOS"]


tmpTriggerList =  [
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0ElectronDecision",
    "L0DiMuonDecision",
    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1GlobalDecision",
    "L0GlobalDecision"
]

LoKiTupleDau = LoKi__Hybrid__TupleTool("LoKi_Dau")
LoKiTupleDau.Variables =  {
  "ETA"        : "ETA"
, "PHI"        : "PHI"
}
LoKiTupleMother  = LoKi__Hybrid__TupleTool("LoKi_Mother")
LoKiTupleMother.Variables =  {
    "DOCA" : "DOCAMAX"
    , "BPVTRGPOINTING" : "BPVTRGPOINTING"
    , "DOCA12" : "DOCA(1,2)"
    , "DOCA23" : "DOCA(2,3)"
    , "DOCA13" : "DOCA(1,3)"
    , "DOCA12_CHI2" : "DOCACHI2(1,2)"
    , "DOCA23_CHI2" : "DOCACHI2(2,3)"
    , "DOCA13_CHI2" : "DOCACHI2(1,3)"
    , "ETA"        : "ETA"
    , "PHI"        : "PHI"

    ,'CONE1ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEANGLE', -10. )"
    ,'CONE1MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEMULT', -10. )"
    ,'CONE1PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEPTASYM', -10. )"
    ,'CONE1ANGLEDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEANGLE', -10. )"
    ,'CONE1MULTDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEMULT', -10. )"
    ,'CONE1PTASYMDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEPTASYM', -10. )"
    ,'CONE2ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEANGLE', -10. )"
    ,'CONE2MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEMULT', -10. )"
    ,'CONE2PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEPTASYM', -10. )"
}
LoKiTupleMother1  = LoKi__Hybrid__TupleTool("LoKi_Mother1")
LoKiTupleMother1.Variables =  {
    "DOCA" : "DOCAMAX"
    , "BPVTRGPOINTING" : "BPVTRGPOINTING"
    , "DOCA12" : "DOCA(2,3)"
    , "DOCA23" : "DOCA(3,1)"
    , "DOCA13" : "DOCA(2,1)"
    , "DOCA12_CHI2" : "DOCACHI2(2,3)"
    , "DOCA23_CHI2" : "DOCACHI2(3,1)"
    , "DOCA13_CHI2" : "DOCACHI2(2,1)"
    , "ETA"        : "ETA"
    , "PHI"        : "PHI"

    ,'CONE1ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEANGLE', -10. )"
    ,'CONE1MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEMULT', -10. )"
    ,'CONE1PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEPTASYM', -10. )"

    ,'CONE1ANGLEDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEANGLE', -10. )"
    ,'CONE1MULTDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEMULT', -10. )"
    ,'CONE1PTASYMDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEPTASYM', -10. )"
    ,'CONE2ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEANGLE', -10. )"
    ,'CONE2MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEMULT', -10. )"
    ,'CONE2PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEPTASYM', -10. )"
}
LoKiTupleMother2  = LoKi__Hybrid__TupleTool("LoKi_Mother2")
LoKiTupleMother2.Variables =  {
    "DOCA" : "DOCAMAX"
    , "BPVTRGPOINTING" : "BPVTRGPOINTING"
    , "DOCA12" : "DOCA(3,2)"
    , "DOCA23" : "DOCA(2,1)"
    , "DOCA13" : "DOCA(3,1)"
    , "DOCA12_CHI2" : "DOCACHI2(3,2)"
    , "DOCA23_CHI2" : "DOCACHI2(2,1)"
    , "DOCA13_CHI2" : "DOCACHI2(3,1)"
    , "ETA"        : "ETA"
    , "PHI"        : "PHI"

    ,'CONE1ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEANGLE', -10. )"
    ,'CONE1MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEMULT', -10. )"
    ,'CONE1PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEPTASYM', -10. )"

    ,'CONE1ANGLEDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEANGLE', -10. )"
    ,'CONE1MULTDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEMULT', -10. )"
    ,'CONE1PTASYMDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEPTASYM', -10. )"
    ,'CONE2ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEANGLE', -10. )"
    ,'CONE2MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEMULT', -10. )"
    ,'CONE2PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEPTASYM', -10. )"
}

from Configurables import LoKi__Hybrid__EvtTupleTool
LoKiEVENTVariables = LoKi__Hybrid__EvtTupleTool('LoKiEVENTVariables')
LoKiEVENTVariables.Preambulo = ["from LoKiTracks.decorators import *",
                                 "from LoKiNumbers.decorators import *",
                                 "from LoKiCore.functions import *"
                                ]

# For D2PPK change order of the daughters for DTF
LoKiTuple0 = LoKi__Hybrid__TupleTool("LoKi_D_DTF")
LoKiTuple0.Variables =  {
    "DTF_M"       : "DTF_FUN ( M , True, 'D+' )",
    # Dplus
    "DTF_PX"      : "DTF_FUN ( PX , True, 'D+' )",
    "DTF_PY"      : "DTF_FUN ( PY , True, 'D+' )",
    "DTF_PZ"      : "DTF_FUN ( PZ , True, 'D+' )",
    "DTF_PE"      : "DTF_FUN ( E , True, 'D+' )",
    "DTF_p1_PX"   : "DTF_FUN ( CHILD(2, PX), True, 'D+' )",
    "DTF_p1_PY"   : "DTF_FUN ( CHILD(2, PY), True, 'D+' )",
    "DTF_p1_PZ"   : "DTF_FUN ( CHILD(2, PZ), True, 'D+' )",
    "DTF_p1_PE"   : "DTF_FUN ( CHILD(2, E), True, 'D+' )",
    "DTF_p2_PX"   : "DTF_FUN ( CHILD(3, PX), True, 'D+' )",
    "DTF_p2_PY"   : "DTF_FUN ( CHILD(3, PY), True, 'D+' )",
    "DTF_p2_PZ"   : "DTF_FUN ( CHILD(3, PZ), True, 'D+' )",
    "DTF_p2_PE"   : "DTF_FUN ( CHILD(3, E), True, 'D+' )",
    "DTF_p3_PX"   : "DTF_FUN ( CHILD(1, PX), True, 'D+' )",
    "DTF_p3_PY"   : "DTF_FUN ( CHILD(1, PY), True, 'D+' )",
    "DTF_p3_PZ"   : "DTF_FUN ( CHILD(1, PZ), True, 'D+' )",
    "DTF_p3_PE"   : "DTF_FUN ( CHILD(1, E), True, 'D+' )",

#    "DTF_CHI2"    : "DTF_CHI2( True, 'D+' )",
#    "DTF_NDOF"    : "DTF_NDOF( True, 'D+' )",
    "DTF_CHI2NDOF": "DTF_CHI2NDOF( True, 'D+' )"
# constrain only in vertex
,   "DTF_VTX_M"       : "DTF_FUN ( M , True )",
    "DTF_VTX_PX"      : "DTF_FUN ( PX , True )",
    "DTF_VTX_PY"      : "DTF_FUN ( PY , True )",
    "DTF_VTX_PZ"      : "DTF_FUN ( PZ , True )",
    "DTF_VTX_PE"      : "DTF_FUN ( E , True )",
    "DTF_VTX_p1_PX"   : "DTF_FUN ( CHILD(2, PX), True )",
    "DTF_VTX_p1_PY"   : "DTF_FUN ( CHILD(2, PY), True )",
    "DTF_VTX_p1_PZ"   : "DTF_FUN ( CHILD(2, PZ), True )",
    "DTF_VTX_p1_PE"   : "DTF_FUN ( CHILD(2, E), True )",
    "DTF_VTX_p2_PX"   : "DTF_FUN ( CHILD(3, PX), True )",
    "DTF_VTX_p2_PY"   : "DTF_FUN ( CHILD(3, PY), True )",
    "DTF_VTX_p2_PZ"   : "DTF_FUN ( CHILD(3, PZ), True )",
    "DTF_VTX_p2_PE"   : "DTF_FUN ( CHILD(3, E), True )",
    "DTF_VTX_p3_PX"   : "DTF_FUN ( CHILD(1, PX), True )",
    "DTF_VTX_p3_PY"   : "DTF_FUN ( CHILD(1, PY), True )",
    "DTF_VTX_p3_PZ"   : "DTF_FUN ( CHILD(1, PZ), True )",
    "DTF_VTX_p3_PE"   : "DTF_FUN ( CHILD(1, E), True )",

#    "DTF_VTX_CHI2"    : "DTF_CHI2( True )",
#    "DTF_VTX_NDOF"    : "DTF_NDOF( True )",
    "DTF_VTX_CHI2NDOF": "DTF_CHI2NDOF( True )"
    }
# For Ds2PPK,DsPKK change order of the daughters for DTF
LoKiTuple1 = LoKi__Hybrid__TupleTool("LoKi_Ds_DTF1")
LoKiTuple1.Variables =  {
    "DTF_M"       : "DTF_FUN ( M , True, 'D_s+' )",
    # Dplus
    "DTF_PX"      : "DTF_FUN ( PX , True, 'D_s+' )",
    "DTF_PY"      : "DTF_FUN ( PY , True, 'D_s+' )",
    "DTF_PZ"      : "DTF_FUN ( PZ , True, 'D_s+' )",
    "DTF_PE"      : "DTF_FUN ( E , True, 'D_s+' )",
    "DTF_p1_PX"   : "DTF_FUN ( CHILD(2, PX), True, 'D_s+' )",
    "DTF_p1_PY"   : "DTF_FUN ( CHILD(2, PY), True, 'D_s+' )",
    "DTF_p1_PZ"   : "DTF_FUN ( CHILD(2, PZ), True, 'D_s+' )",
    "DTF_p1_PE"   : "DTF_FUN ( CHILD(2, E), True, 'D_s+' )",
    "DTF_p2_PX"   : "DTF_FUN ( CHILD(3, PX), True, 'D_s+' )",
    "DTF_p2_PY"   : "DTF_FUN ( CHILD(3, PY), True, 'D_s+' )",
    "DTF_p2_PZ"   : "DTF_FUN ( CHILD(3, PZ), True, 'D_s+' )",
    "DTF_p2_PE"   : "DTF_FUN ( CHILD(3, E), True, 'D_s+' )",
    "DTF_p3_PX"   : "DTF_FUN ( CHILD(1, PX), True, 'D_s+' )",
    "DTF_p3_PY"   : "DTF_FUN ( CHILD(1, PY), True, 'D_s+' )",
    "DTF_p3_PZ"   : "DTF_FUN ( CHILD(1, PZ), True, 'D_s+' )",
    "DTF_p3_PE"   : "DTF_FUN ( CHILD(1, E), True, 'D_s+' )",

#    "DTF_CHI2"    : "DTF_CHI2( True, 'D_s+' )",
#    "DTF_NDOF"    : "DTF_NDOF( True, 'D_s+' )",
    "DTF_CHI2NDOF": "DTF_CHI2NDOF( True, 'D_s+' )"
# constrain only in vertex
,   "DTF_VTX_M"       : "DTF_FUN ( M , True )",
    "DTF_VTX_PX"      : "DTF_FUN ( PX , True )",
    "DTF_VTX_PY"      : "DTF_FUN ( PY , True )",
    "DTF_VTX_PZ"      : "DTF_FUN ( PZ , True )",
    "DTF_VTX_PE"      : "DTF_FUN ( E , True )",
    "DTF_VTX_p1_PX"   : "DTF_FUN ( CHILD(2, PX), True )",
    "DTF_VTX_p1_PY"   : "DTF_FUN ( CHILD(2, PY), True )",
    "DTF_VTX_p1_PZ"   : "DTF_FUN ( CHILD(2, PZ), True )",
    "DTF_VTX_p1_PE"   : "DTF_FUN ( CHILD(2, E), True )",
    "DTF_VTX_p2_PX"   : "DTF_FUN ( CHILD(3, PX), True )",
    "DTF_VTX_p2_PY"   : "DTF_FUN ( CHILD(3, PY), True )",
    "DTF_VTX_p2_PZ"   : "DTF_FUN ( CHILD(3, PZ), True )",
    "DTF_VTX_p2_PE"   : "DTF_FUN ( CHILD(3, E), True )",
    "DTF_VTX_p3_PX"   : "DTF_FUN ( CHILD(1, PX), True )",
    "DTF_VTX_p3_PY"   : "DTF_FUN ( CHILD(1, PY), True )",
    "DTF_VTX_p3_PZ"   : "DTF_FUN ( CHILD(1, PZ), True )",
    "DTF_VTX_p3_PE"   : "DTF_FUN ( CHILD(1, E), True )",

#    "DTF_VTX_CHI2"    : "DTF_CHI2( True )",
#    "DTF_VTX_NDOF"    : "DTF_NDOF( True )",
    "DTF_VTX_CHI2NDOF": "DTF_CHI2NDOF( True )"
    }
LoKiTuple2 = LoKi__Hybrid__TupleTool("LoKi_Ds_DTF2")
LoKiTuple2.Variables =  {
    "DTF_M"       : "DTF_FUN ( M , True, 'D_s+' )",
    # Dplus
    "DTF_PX"      : "DTF_FUN ( PX , True, 'D_s+' )",
    "DTF_PY"      : "DTF_FUN ( PY , True, 'D_s+' )",
    "DTF_PZ"      : "DTF_FUN ( PZ , True, 'D_s+' )",
    "DTF_PE"      : "DTF_FUN ( E , True, 'D_s+' )",
    "DTF_p1_PX"   : "DTF_FUN ( CHILD(3, PX), True, 'D_s+' )",
    "DTF_p1_PY"   : "DTF_FUN ( CHILD(3, PY), True, 'D_s+' )",
    "DTF_p1_PZ"   : "DTF_FUN ( CHILD(3, PZ), True, 'D_s+' )",
    "DTF_p1_PE"   : "DTF_FUN ( CHILD(3, E), True, 'D_s+' )",
    "DTF_p2_PX"   : "DTF_FUN ( CHILD(2, PX), True, 'D_s+' )",
    "DTF_p2_PY"   : "DTF_FUN ( CHILD(2, PY), True, 'D_s+' )",
    "DTF_p2_PZ"   : "DTF_FUN ( CHILD(2, PZ), True, 'D_s+' )",
    "DTF_p2_PE"   : "DTF_FUN ( CHILD(2, E), True, 'D_s+' )",
    "DTF_p3_PX"   : "DTF_FUN ( CHILD(1, PX), True, 'D_s+' )",
    "DTF_p3_PY"   : "DTF_FUN ( CHILD(1, PY), True, 'D_s+' )",
    "DTF_p3_PZ"   : "DTF_FUN ( CHILD(1, PZ), True, 'D_s+' )",
    "DTF_p3_PE"   : "DTF_FUN ( CHILD(1, E), True, 'D_s+' )",

#    "DTF_CHI2"    : "DTF_CHI2( True, 'D_s+' )",
#    "DTF_NDOF"    : "DTF_NDOF( True, 'D_s+' )",
    "DTF_CHI2NDOF": "DTF_CHI2NDOF( True, 'D_s+' )"
# constrain only in vertex
,   "DTF_VTX_M"       : "DTF_FUN ( M , True )",
    "DTF_VTX_PX"      : "DTF_FUN ( PX , True )",
    "DTF_VTX_PY"      : "DTF_FUN ( PY , True )",
    "DTF_VTX_PZ"      : "DTF_FUN ( PZ , True )",
    "DTF_VTX_PE"      : "DTF_FUN ( E , True )",
    "DTF_VTX_p1_PX"   : "DTF_FUN ( CHILD(3, PX), True )",
    "DTF_VTX_p1_PY"   : "DTF_FUN ( CHILD(3, PY), True )",
    "DTF_VTX_p1_PZ"   : "DTF_FUN ( CHILD(3, PZ), True )",
    "DTF_VTX_p1_PE"   : "DTF_FUN ( CHILD(3, E), True )",
    "DTF_VTX_p2_PX"   : "DTF_FUN ( CHILD(2, PX), True )",
    "DTF_VTX_p2_PY"   : "DTF_FUN ( CHILD(2, PY), True )",
    "DTF_VTX_p2_PZ"   : "DTF_FUN ( CHILD(2, PZ), True )",
    "DTF_VTX_p2_PE"   : "DTF_FUN ( CHILD(2, E), True )",
    "DTF_VTX_p3_PX"   : "DTF_FUN ( CHILD(1, PX), True )",
    "DTF_VTX_p3_PY"   : "DTF_FUN ( CHILD(1, PY), True )",
    "DTF_VTX_p3_PZ"   : "DTF_FUN ( CHILD(1, PZ), True )",
    "DTF_VTX_p3_PE"   : "DTF_FUN ( CHILD(1, E), True )",

#    "DTF_VTX_CHI2"    : "DTF_CHI2( True )",
#    "DTF_VTX_NDOF"    : "DTF_NDOF( True )",
    "DTF_VTX_CHI2NDOF": "DTF_CHI2NDOF( True )"
    }
LoKiTuple3 = LoKi__Hybrid__TupleTool("LoKi_D")
LoKiTuple3.Variables =  {
    "DTF_M"       : "DTF_FUN ( M , True, 'D+' )",
    # Dplus
    "DTF_PX"      : "DTF_FUN ( PX , True, 'D+' )",
    "DTF_PY"      : "DTF_FUN ( PY , True, 'D+' )",
    "DTF_PZ"      : "DTF_FUN ( PZ , True, 'D+' )",
    "DTF_PE"      : "DTF_FUN ( E , True, 'D+' )",
    "DTF_p1_PX"   : "DTF_FUN ( CHILD(1, PX), True, 'D+' )",
    "DTF_p1_PY"   : "DTF_FUN ( CHILD(1, PY), True, 'D+' )",
    "DTF_p1_PZ"   : "DTF_FUN ( CHILD(1, PZ), True, 'D+' )",
    "DTF_p1_PE"   : "DTF_FUN ( CHILD(1, E), True, 'D+' )",
    "DTF_p2_PX"   : "DTF_FUN ( CHILD(2, PX), True, 'D+' )",
    "DTF_p2_PY"   : "DTF_FUN ( CHILD(2, PY), True, 'D+' )",
    "DTF_p2_PZ"   : "DTF_FUN ( CHILD(2, PZ), True, 'D+' )",
    "DTF_p2_PE"   : "DTF_FUN ( CHILD(2, E), True, 'D+' )",
    "DTF_p3_PX"   : "DTF_FUN ( CHILD(3, PX), True, 'D+' )",
    "DTF_p3_PY"   : "DTF_FUN ( CHILD(3, PY), True, 'D+' )",
    "DTF_p3_PZ"   : "DTF_FUN ( CHILD(3, PZ), True, 'D+' )",
    "DTF_p3_PE"   : "DTF_FUN ( CHILD(3, E), True, 'D+' )",

#    "DTF_CHI2"    : "DTF_CHI2( True, 'D+' )",
#    "DTF_NDOF"    : "DTF_NDOF( True, 'D+' )",
    "DTF_CHI2NDOF": "DTF_CHI2NDOF( True, 'D+' )"
# constrain only in vertex
,   "DTF_VTX_M"       : "DTF_FUN ( M , True )",
    "DTF_VTX_PX"      : "DTF_FUN ( PX , True )",
    "DTF_VTX_PY"      : "DTF_FUN ( PY , True )",
    "DTF_VTX_PZ"      : "DTF_FUN ( PZ , True )",
    "DTF_VTX_PE"      : "DTF_FUN ( E , True )",
    "DTF_VTX_p1_PX"   : "DTF_FUN ( CHILD(1, PX), True )",
    "DTF_VTX_p1_PY"   : "DTF_FUN ( CHILD(1, PY), True )",
    "DTF_VTX_p1_PZ"   : "DTF_FUN ( CHILD(1, PZ), True )",
    "DTF_VTX_p1_PE"   : "DTF_FUN ( CHILD(1, E), True )",
    "DTF_VTX_p2_PX"   : "DTF_FUN ( CHILD(2, PX), True )",
    "DTF_VTX_p2_PY"   : "DTF_FUN ( CHILD(2, PY), True )",
    "DTF_VTX_p2_PZ"   : "DTF_FUN ( CHILD(2, PZ), True )",
    "DTF_VTX_p2_PE"   : "DTF_FUN ( CHILD(2, E), True )",
    "DTF_VTX_p3_PX"   : "DTF_FUN ( CHILD(3, PX), True )",
    "DTF_VTX_p3_PY"   : "DTF_FUN ( CHILD(3, PY), True )",
    "DTF_VTX_p3_PZ"   : "DTF_FUN ( CHILD(3, PZ), True )",
    "DTF_VTX_p3_PE"   : "DTF_FUN ( CHILD(3, E), True )",

#    "DTF_VTX_CHI2"    : "DTF_CHI2( True )",
#    "DTF_VTX_NDOF"    : "DTF_NDOF( True )",
    "DTF_VTX_CHI2NDOF": "DTF_CHI2NDOF( True )"
    }
LoKiTuple4 = LoKi__Hybrid__TupleTool("LoKi_Ds")
LoKiTuple4.Variables =  {
    "DTF_M"       : "DTF_FUN ( M , True, 'D_s+' )",
    # Dplus
    "DTF_PX"      : "DTF_FUN ( PX , True, 'D_s+' )",
    "DTF_PY"      : "DTF_FUN ( PY , True, 'D_s+' )",
    "DTF_PZ"      : "DTF_FUN ( PZ , True, 'D_s+' )",
    "DTF_PE"      : "DTF_FUN ( E , True, 'D_s+' )",
    "DTF_p1_PX"   : "DTF_FUN ( CHILD(1, PX), True, 'D_s+' )",
    "DTF_p1_PY"   : "DTF_FUN ( CHILD(1, PY), True, 'D_s+' )",
    "DTF_p1_PZ"   : "DTF_FUN ( CHILD(1, PZ), True, 'D_s+' )",
    "DTF_p1_PE"   : "DTF_FUN ( CHILD(1, E), True, 'D_s+' )",
    "DTF_p2_PX"   : "DTF_FUN ( CHILD(2, PX), True, 'D_s+' )",
    "DTF_p2_PY"   : "DTF_FUN ( CHILD(2, PY), True, 'D_s+' )",
    "DTF_p2_PZ"   : "DTF_FUN ( CHILD(2, PZ), True, 'D_s+' )",
    "DTF_p2_PE"   : "DTF_FUN ( CHILD(2, E), True, 'D_s+' )",
    "DTF_p3_PX"   : "DTF_FUN ( CHILD(3, PX), True, 'D_s+' )",
    "DTF_p3_PY"   : "DTF_FUN ( CHILD(3, PY), True, 'D_s+' )",
    "DTF_p3_PZ"   : "DTF_FUN ( CHILD(3, PZ), True, 'D_s+' )",
    "DTF_p3_PE"   : "DTF_FUN ( CHILD(3, E), True, 'D_s+' )",

#    "DTF_CHI2"    : "DTF_CHI2( True, 'D_s+' )",
#    "DTF_NDOF"    : "DTF_NDOF( True, 'D_s+' )",
    "DTF_CHI2NDOF": "DTF_CHI2NDOF( True, 'D_s+' )"
# constrain only in vertex
,   "DTF_VTX_M"       : "DTF_FUN ( M , True )",
    "DTF_VTX_PX"      : "DTF_FUN ( PX , True )",
    "DTF_VTX_PY"      : "DTF_FUN ( PY , True )",
    "DTF_VTX_PZ"      : "DTF_FUN ( PZ , True )",
    "DTF_VTX_PE"      : "DTF_FUN ( E , True )",
    "DTF_VTX_p1_PX"   : "DTF_FUN ( CHILD(1, PX), True )",
    "DTF_VTX_p1_PY"   : "DTF_FUN ( CHILD(1, PY), True )",
    "DTF_VTX_p1_PZ"   : "DTF_FUN ( CHILD(1, PZ), True )",
    "DTF_VTX_p1_PE"   : "DTF_FUN ( CHILD(1, E), True )",
    "DTF_VTX_p2_PX"   : "DTF_FUN ( CHILD(2, PX), True )",
    "DTF_VTX_p2_PY"   : "DTF_FUN ( CHILD(2, PY), True )",
    "DTF_VTX_p2_PZ"   : "DTF_FUN ( CHILD(2, PZ), True )",
    "DTF_VTX_p2_PE"   : "DTF_FUN ( CHILD(2, E), True )",
    "DTF_VTX_p3_PX"   : "DTF_FUN ( CHILD(3, PX), True )",
    "DTF_VTX_p3_PY"   : "DTF_FUN ( CHILD(3, PY), True )",
    "DTF_VTX_p3_PZ"   : "DTF_FUN ( CHILD(3, PZ), True )",
    "DTF_VTX_p3_PE"   : "DTF_FUN ( CHILD(3, E), True )",

#    "DTF_VTX_CHI2"    : "DTF_CHI2( True )",
#    "DTF_VTX_NDOF"    : "DTF_NDOF( True )",
    "DTF_VTX_CHI2NDOF": "DTF_CHI2NDOF( True )"

    }

from Gaudi.Configuration import *
from PhysConf.Selections import Selection, AutomaticData, PrintSelection, MomentumScaling 


mysel = AutomaticData   (Dhhh_line+"/Particles")
#mysel = PrintSelection  ( mysel )
mysel = MomentumScaling ( mysel, Turbo = True , Year = year )  

ntp1 = DecayTreeTuple(Dhhh_Tuplename)
ntp1.Decay = Dhhh_decay
ntp1.addBranches  (Dhhh_branches)
ntp1.TupleName = "ntp1"

if( year=='2015'): 
  ntp1.WriteP2PVRelations = False
  #ntp1.IgnoreP2PVFromInputLocations = True
  ntp1.InputPrimaryVertices = "/Event/Turbo/Primary" 


ntp1.Inputs = [ mysel.outputLocation() ]


ntp1.ToolList += tmpToolList 
#ntp1.addTool(TupleToolPrimaries)
#ntp1.TupleToolPrimaries.InputLocation= "/Event/Turbo/Primary"
ntp1.addTool(LoKiEVENTVariables , name = 'LoKiEVENTVariables' )

ntp1.D.ToolList = motherToolList
if (motherDTF == 'D_DTF' and mother == 'D') : ntp1.D.addTool(LoKiTupleMother1)
if (motherDTF == 'Ds_DTF1' and mother == 'Ds') : ntp1.D.addTool(LoKiTupleMother1)
if (motherDTF == 'Ds_DTF2' and mother == 'Ds') : ntp1.D.addTool(LoKiTupleMother2)
if (motherDTF == 'D' and mother == 'D') : ntp1.D.addTool(LoKiTupleMother)
if (motherDTF == 'Ds' and mother == 'Ds') : ntp1.D.addTool(LoKiTupleMother)
ntp1.D.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_"+motherDTF]
if (motherDTF == 'D_DTF' and mother == 'D') : ntp1.D.addTool(LoKiTuple0)
if (motherDTF == 'Ds_DTF1' and mother == 'Ds') : ntp1.D.addTool(LoKiTuple1)
if (motherDTF == 'Ds_DTF2' and mother == 'Ds') : ntp1.D.addTool(LoKiTuple2)
if (motherDTF == 'D' and mother == 'D') : ntp1.D.addTool(LoKiTuple3)
if (motherDTF == 'Ds' and mother == 'Ds') : ntp1.D.addTool(LoKiTuple4)


tttt = TupleToolTISTOS()
tttt.addTool(L0TriggerTisTos())
tttt.addTool(TriggerTisTos())
ntp1.D.addTool(tttt)

ntp1.D.TupleToolTISTOS.Verbose = True
ntp1.D.TupleToolTISTOS.TriggerList = tmpTriggerList
ntp1.D.TupleToolTISTOS.TriggerTisTos.TOSFracEcal = 0.
ntp1.D.TupleToolTISTOS.TriggerTisTos.TOSFracHcal = 0.
ntp1.D.TupleToolTISTOS.FillHlt2 = False


#small class for the daughters variable
def mySharedConf(branch):
  branch.ToolList = daugToolList
  branch.addTool(LoKiTupleDau)


mySharedConf(ntp1.p1)
mySharedConf(ntp1.p2)
mySharedConf(ntp1.p3)

#=================

# Necessary DaVinci parameters #################
from Configurables import LHCbApp
LHCbApp().XMLSummary = 'summary.xml'

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
        VOID_Code = """
            0 < CONTAINS('%s/%s'+'/Particles')
                """ % ( rootInTES , Dhhh_line )
            )
DaVinci().EventPreFilters = fltrs.filters('FILTER')

DaVinci().Simulation   = False
DaVinci().SkipEvents = 0
DaVinci().EvtMax = -1
DaVinci().Lumi = True
DaVinci().TupleFile = 'TeslaTuplesNoCuts.root'
DaVinci().PrintFreq = 10000
DaVinci().UserAlgorithms = [] 
DaVinci().UserAlgorithms.append (mysel)
DaVinci().UserAlgorithms.append (ntp1)

DaVinci().InputType = 'MDST'
DaVinci().RootInTES = rootInTES

DaVinci().Turbo = True
from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = year)

from Gaudi.Configuration import *
#To get the PFN:
#lb-run LHCbDirac/prod bash 
#dirac-dms-lfn-accessURL --Protocol=root /lhcb/validation/Collision18/CHARMCHARGED.MDST/00074489/0000/00074489_00000841_1.charmcharged.mdst
#For Turbo 2018 the location is  /validation/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo05/94000000/CHARMCHARGED.MDST
#/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076443/0000/00076443_00006569_1.charmcharged.mdst ---> failing.
# there are two replicas of this:
#root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076443/0000/00076443_00006569_1.charmcharged.mdst
#root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076443/0000/00076443_00006569_1.charmcharged.mdst

#To run locally:
#lb-run LHCbDirac/prod bash
#lb-run DaVinci/latest gaudirun.py D2hhh_Run2Tuples.py > log2

#To see the locations in a mdst:
# lb-run Bender/latest  dst-dump   -n 100 -f PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMSPEC.MDST/00075226/0001/00075226_00012228_1.charmspec.mdst  

from GaudiConf.IOHelper import IOHelper
if (year=='2015'):
  IOHelper().inputFiles( [
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000001_1.turbo.mdst',
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000002_1.turbo.mdst',
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000003_1.turbo.mdst',
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000004_1.turbo.mdst',
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000005_1.turbo.mdst',
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000006_1.turbo.mdst',
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/TURBO.MDST/00051289/0000/00051289_00000007_1.turbo.mdst'])  #2015
if (year=='2016'):
  if (channel == 'Ds2KKP' or channel == 'D2KPP') :
    IOHelper().inputFiles( ['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMSPECPARKED.MDST/00076512/0001/00076512_00010297_1.charmspecparked.mdst',
  'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMSPECPARKED.MDST/00076441/0000/00076441_00000670_1.charmspecparked.mdst',  
   'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMSPECPARKED.MDST/00076441/0000/00076441_00000740_1.charmspecparked.mdst', ])  #2016 Charmspecparked (KPP DsKKP)
  if (channel=='D2KKP' or channel=='D2PPP' or channel=='D2PPK' or channel=='D2KKK' or channel=='Ds2PPP' or channel=='Ds2PPK' or channel=='Ds2KKK' or channel=='Ds2PKK'):
    IOHelper().inputFiles( [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076439/0000/00076439_00000008_1.charmcharged.mdst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076439/0000/00076439_00009919_1.charmcharged.mdst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076439/0000/00076439_00009966_1.charmcharged.mdst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076439/0000/00076439_00009991_1.charmcharged.mdst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCHARGED.MDST/00076439/0000/00076439_00009999_1.charmcharged.mdst'])  #2016 CharmCharged
if (year=='2017'):
  if (channel == 'Ds2KKP' or channel == 'D2KPP') :
    IOHelper().inputFiles( ['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARMSPEC.MDST/00064380/0000/00064380_00000120_1.charmspec.mdst'])  #2017 charmspec (KPP DsKKP)
  if (channel=='D2KKP' or channel=='D2PPP' or channel=='D2PPK' or channel=='D2KKK' or channel=='Ds2PPP' or channel=='Ds2PPK' or channel=='Ds2KKK' or channel=='Ds2PKK'):
    IOHelper().inputFiles( ['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARMCHARGED.MDST/00064380/0000/00064380_00000352_1.charmcharged.mdst'])  #2017 CharmCharged 
if (year=='2018'):
  if (channel == 'Ds2KKP' or channel == 'D2KPP') :
    IOHelper().inputFiles( ['PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMSPEC.MDST/00080042/0000/00080042_00002457_1.charmspec.mdst'])  #2018 charmspec (KPP DsKKP)
  if (channel=='D2KKP' or channel=='D2PPP' or channel=='D2PPK' or channel=='D2KKK' or channel=='Ds2PPP' or channel=='Ds2PPK' or channel=='Ds2KKK' or channel=='Ds2PKK'):
    IOHelper().inputFiles( ['PFN:root://ccdcacli264.in2p3.fr:1094/pnfs/in2p3.fr/data/lhcb/LHCb/Collision18/CHARMCHARGED.MDST/00080042/0000/00080042_00003218_1.charmcharged.mdst']) #2018 CharmCharged 

