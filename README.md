This is the repository for the code used in the D2hhh analyses in Rio/Bogota/Warsow 

WGProduction:
scripts to produce ntuples for 2016-2018 data, with and without cuts, locally or in the grid.

The scripts here serve as a basis for the scripts we will add to the CharmWGProd package

ntpmakers:
root scripts to reduce the size of the grid ntuples


**How to Run the scripts in WGProduction:**

**--> to run locally:**
Do:
lhcb-proxy-init
lb-run DaVinci/latest gaudirun.py DataType-2016.py DecayType-KKP.py D2hhh_Run2Tuples-NoCuts.py > KKP_NoCuts.log
or
lb-run DaVinci/latest gaudirun.py DataType-2016.py DecayType-KKP.py D2hhh_Run2Tuples-WithCuts.py > KKP_WithCuts.log

**(always check line DaVinci().EvtMax to guarantee you run over the desired number of events)**

There is one DataType-20**.py for each run year and one DecayType-*.py for each D or Ds decay

**--> to run the whole set on the GRID:**

1.  edit the Ganga_D2hhh_Run2Tuples_NoCuts.py 
    comment and uncommenting the relevant lines:
    *   to run it for the first time ever, uncomment the line 
#APPLICATION = prepareGaudiExec('DaVinci', 'v44r10', myPath='.')
and comment the following two :
APPLICATION = GaudiExec()
APPLICATION.directory = "./DaVinciDev_v44r10"
It will create a DaVinciDev_v44r10 directory. The next time you submit any other job, do the opposite 
(comment out the first and uncomment the other two).
    *   choose the DataType-year.py file
    *   choose the DecayType-decay.py file
    *   choose the NAME = '2016_DsPPP_Up'  accordingly just for future monitoring
    *   Choose the data files to be run.  It is the line that defines the bkq.path 
        In this line you will have to  change MagDown to MagUp too.
    
2.  To run:
ganga  Ganga_D2hhh_Run2Tuples_NoCuts.py

(Instructions above are identical for file Ganga_D2hhh_Run2Tuples_WithCuts.py )
**(always check line DaVinci().EvtMax in D2hhh_Run2Tuples-NoCuts-Ganga.py or D2hhh_Run2Tuples-WithCuts-Ganga.py to guarantee you run over the desired number of events)**

3.  Monitor your jobs. (I can send instructions if needed)
Once the job is completed, we need:

4.  Make sure that it has processed every file: 
copy the script ~sandra/cmtuser/Run2-scripts/makeSummaryTurbo.py  to your 
gangadir/workspace/[yourUserName]/LocalXML
    then do:
 ./makeSummaryTurbo.py  [jobNumber]  [channel] [NumberOfSubjobs] 
 for example: ./makeSummaryTurbo.py 664 KKK  91
(possible channels are: KKP, PPP, KPP, KPPos, KKK, KKPos, DsKKP, DsPPP etc 
it will print the number of input events, which you need to compare to what is in the bkk. 

Data in the bkk is listed in DadosRunII.txt for each year


