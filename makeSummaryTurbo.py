#!/usr/bin/python

"Read  summary.xml from each subjob of job and summ up for given keys"
import os
import sys
job = sys.argv[1]
channel = sys.argv[2]
njobs = sys.argv[3]
sj=-1
nfailed=0
nxml=0
nok=0
print njobs
dir = str(job)+"/"+str(sj)
head = "----------------> Summary for job "+str(job)
print head 
summary={'ntp/Event':0}
empty = " "
suminputEvt = 0
while True:
  sj=sj+1
  if str(sj)==str(njobs):
    break
  dir = str(job)+"/"+str(sj)
  if not os.access(dir, os.F_OK):
    #print 'not os.access subjob '+dir 
    nfailed=nfailed+1
    #break
    continue
  file=dir +'/output/summary.xml'
  #print file
  if  not os.path.isfile(file):
    #print 'no '+file
    nxml=nxml+1
    continue
  nok=nok+1  
  f = open(file,'r')
  #print file
  lumi=0.;
  errlumi=0.;



  for line in f:
   if  line.find("full") != -1:
     inputEvt = float((line.split('>')[1]).split('<')[0])
     suminputEvt =  suminputEvt + inputEvt
  f.close()
  f = open(file,'r')

  found = False
  for line in f:
    if line.find("ntp_"+channel+"/Event") != -1:
      found = True
      nEvents=int((line.split('>')[1]).split('<')[0])
      summary["ntp/Event"]=summary["ntp/Event"]+nEvents
      #print "  Events ",nEvents," Total: ", summary["DaVinciInit/Events"]
      break
  if not found:
    #print "Warning: No event analised in subjob ",sj
    empty=empty+str(sj)+", "
    continue
  for line in f:
    if line.find("nWritten") != -1:
      tag = (line.split('>')[0]).split('=')[1]
      nWritten=int((line.split('>')[1]).split('<')[0])
      if tag in summary:
       summary[tag]=summary[tag] + nWritten
      else:
       summary[tag]=nWritten
      #print "  ",tag,nWritten, " Total: ", summary[tag]
    if line.find("IntegrateBeamCrossing/Luminosity") != -1:
      #print line
      tag1 = (line.split('>')[0]).split('=')[4]
      lumi=float((line.split('>')[1]).split(' 1.0 ')[0])
      errlumi=float((line.split('>')[1]).split(' 1.0 ')[1].split('<')[0])

      if tag1 in summary:
       summary[tag1]=summary[tag1] + lumi    
      else:
       summary[tag1]=lumi
       
      tag2 = tag1+"Err2"
      if tag2 in summary:
       summary[tag2]=summary[tag2] + errlumi*errlumi    
      else:
       summary[tag2]=errlumi*errlumi

  #raw_input()
  f.close()

print "# of files: ", sj
print "# of non existing: ", nfailed, '(no subdir found)'
print "# of failed: ", nxml, '(no summary.xml found)'
print "# of ok:     ", nok     


#f = open(str(job)+'/output/AllDST_list.py','r')
#for line in f:
#  if line.find("Path:")  != -1:
#    print line
#  if line.find("Continuation")  != -1:
#    print line
#  if line.find("DATAFILE=")  != -1:
#    break
#
print "input, %d "%( suminputEvt)

print summary	  
if len(empty) > 1:
  print "\nList of empty subjobs: [",empty.strip(', '),"]"
print "---------------------------------------------------------------"

