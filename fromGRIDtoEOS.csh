#!/bin/csh
set job=$1
set toDir=/eos/lhcb/user/s/sandra/Job${job}/
echo "toDir: ${toDir}"
set filelist=`more LFNslist_job${job}.py`
#echo $filelist
set subjob=0
set step=1
set fromFile=TeslaTuples.root
foreach file ( $filelist )
  echo Trying - subjob ${subjob} - file ${file}
  set toSubjobDir=${toDir}$subjob/
  set toFile = ${toSubjobDir}${fromFile}
  set dummy=`eos ls $toFile`
  set st = $status
  echo $st
  if($st == 0) then
    echo "File $toFile already exists. Skipping."
    @ subjob = ( $subjob + $step )
    continue
  endif
  lb-run LHCbDirac dirac-dms-get-file $file
  eos mkdir -p $toSubjobDir
  xrdcp ${fromFile} root://eoslhcb.cern.ch/$toSubjobDir
  rm -f ${fromFile}
  @ subjob = ( $subjob + $step )
end
echo "Finished transfer"
exit

