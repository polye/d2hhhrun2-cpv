//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Mar 25 15:49:54 2019 by ROOT version 6.12/04
// from TChain ntp_KPP/ntp1/
//////////////////////////////////////////////////////////

#ifndef ntpmakerRun2_h
#define ntpmakerRun2_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <sstream>
#include <iostream>

// Header file for the classes stored in the TTree if any.

class ntpmakerRun2 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   //static constexpr Int_t kMaxD_ENDVERTEX_COV = 1;
   //static constexpr Int_t kMaxD_OWNPV_COV = 1;
   //static constexpr Int_t kMaxp1_OWNPV_COV = 1;
   //static constexpr Int_t kMaxp1_ORIVX_COV = 1;
   //static constexpr Int_t kMaxp2_OWNPV_COV = 1;
   //static constexpr Int_t kMaxp2_ORIVX_COV = 1;
   //static constexpr Int_t kMaxp3_OWNPV_COV = 1;
   //static constexpr Int_t kMaxp3_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        D_DTF_CHI2NDOF;
   Double_t        D_DTF_M;
   Double_t        D_DTF_PE;
   Double_t        D_DTF_PX;
   Double_t        D_DTF_PY;
   Double_t        D_DTF_PZ;
   Double_t        D_DTF_VTX_CHI2NDOF;
   Double_t        D_DTF_VTX_M;
   Double_t        D_DTF_VTX_PE;
   Double_t        D_DTF_VTX_PX;
   Double_t        D_DTF_VTX_PY;
   Double_t        D_DTF_VTX_PZ;
   Double_t        D_DTF_VTX_p1_PE;
   Double_t        D_DTF_VTX_p1_PX;
   Double_t        D_DTF_VTX_p1_PY;
   Double_t        D_DTF_VTX_p1_PZ;
   Double_t        D_DTF_VTX_p2_PE;
   Double_t        D_DTF_VTX_p2_PX;
   Double_t        D_DTF_VTX_p2_PY;
   Double_t        D_DTF_VTX_p2_PZ;
   Double_t        D_DTF_VTX_p3_PE;
   Double_t        D_DTF_VTX_p3_PX;
   Double_t        D_DTF_VTX_p3_PY;
   Double_t        D_DTF_VTX_p3_PZ;
   Double_t        D_DTF_p1_PE;
   Double_t        D_DTF_p1_PX;
   Double_t        D_DTF_p1_PY;
   Double_t        D_DTF_p1_PZ;
   Double_t        D_DTF_p2_PE;
   Double_t        D_DTF_p2_PX;
   Double_t        D_DTF_p2_PY;
   Double_t        D_DTF_p2_PZ;
   Double_t        D_DTF_p3_PE;
   Double_t        D_DTF_p3_PX;
   Double_t        D_DTF_p3_PY;
   Double_t        D_DTF_p3_PZ;
   Double_t        D_BPVTRGPOINTING;
   Double_t        D_CONE1ANGLE;
   Double_t        D_CONE1ANGLEDaughter;
   Double_t        D_CONE1MULT;
   Double_t        D_CONE1MULTDaughter;
   Double_t        D_CONE1PTASYM;
   Double_t        D_CONE1PTASYMDaughter;
   Double_t        D_CONE2ANGLE;
   Double_t        D_CONE2MULT;
   Double_t        D_CONE2PTASYM;
   Double_t        D_DOCA;
   Double_t        D_DOCA12;
   Double_t        D_DOCA12_CHI2;
   Double_t        D_DOCA13;
   Double_t        D_DOCA13_CHI2;
   Double_t        D_DOCA23;
   Double_t        D_DOCA23_CHI2;
   Double_t        D_ETA;
   Double_t        D_PHI;
   Double_t        D_ENDVERTEX_X;
   Double_t        D_ENDVERTEX_Y;
   Double_t        D_ENDVERTEX_Z;
   Double_t        D_ENDVERTEX_XERR;
   Double_t        D_ENDVERTEX_YERR;
   Double_t        D_ENDVERTEX_ZERR;
   Double_t        D_ENDVERTEX_CHI2;
   Int_t           D_ENDVERTEX_NDOF;
   Float_t         D_ENDVERTEX_COV_[3][3];
   Double_t        D_OWNPV_X;
   Double_t        D_OWNPV_Y;
   Double_t        D_OWNPV_Z;
   Double_t        D_OWNPV_XERR;
   Double_t        D_OWNPV_YERR;
   Double_t        D_OWNPV_ZERR;
   Double_t        D_OWNPV_CHI2;
   Int_t           D_OWNPV_NDOF;
   Float_t         D_OWNPV_COV_[3][3];
   Double_t        D_IP_OWNPV;
   Double_t        D_IPCHI2_OWNPV;
   Double_t        D_FD_OWNPV;
   Double_t        D_FDCHI2_OWNPV;
   Double_t        D_DIRA_OWNPV;
   Double_t        D_P;
   Double_t        D_PT;
   Double_t        D_PE;
   Double_t        D_PX;
   Double_t        D_PY;
   Double_t        D_PZ;
   Double_t        D_MM;
   Double_t        D_MMERR;
   Double_t        D_M;
   Int_t           D_ID;
   Double_t        D_TAU;
   Double_t        D_TAUERR;
   Double_t        D_TAUCHI2;
   Bool_t          D_L0Global_Dec;
   Bool_t          D_L0Global_TIS;
   Bool_t          D_L0Global_TOS;
   Bool_t          D_Hlt1Global_Dec;
   Bool_t          D_Hlt1Global_TIS;
   Bool_t          D_Hlt1Global_TOS;
   Bool_t          D_Hlt1Phys_Dec;
   Bool_t          D_Hlt1Phys_TIS;
   Bool_t          D_Hlt1Phys_TOS;
   Bool_t          D_L0PhotonDecision_Dec;
   Bool_t          D_L0PhotonDecision_TIS;
   Bool_t          D_L0PhotonDecision_TOS;
   Bool_t          D_L0HadronDecision_Dec;
   Bool_t          D_L0HadronDecision_TIS;
   Bool_t          D_L0HadronDecision_TOS;
   Bool_t          D_L0MuonDecision_Dec;
   Bool_t          D_L0MuonDecision_TIS;
   Bool_t          D_L0MuonDecision_TOS;
   Bool_t          D_L0ElectronDecision_Dec;
   Bool_t          D_L0ElectronDecision_TIS;
   Bool_t          D_L0ElectronDecision_TOS;
   Bool_t          D_L0DiMuonDecision_Dec;
   Bool_t          D_L0DiMuonDecision_TIS;
   Bool_t          D_L0DiMuonDecision_TOS;
   Bool_t          D_L0GlobalDecision_Dec;
   Bool_t          D_L0GlobalDecision_TIS;
   Bool_t          D_L0GlobalDecision_TOS;
   Bool_t          D_Hlt1TrackMVADecision_Dec;
   Bool_t          D_Hlt1TrackMVADecision_TIS;
   Bool_t          D_Hlt1TrackMVADecision_TOS;
   Bool_t          D_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          D_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          D_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          D_Hlt1GlobalDecision_Dec;
   Bool_t          D_Hlt1GlobalDecision_TIS;
   Bool_t          D_Hlt1GlobalDecision_TOS;
   Double_t        p1_MC12TuneV2_ProbNNe;
   Double_t        p1_MC12TuneV2_ProbNNmu;
   Double_t        p1_MC12TuneV2_ProbNNpi;
   Double_t        p1_MC12TuneV2_ProbNNk;
   Double_t        p1_MC12TuneV2_ProbNNp;
   Double_t        p1_MC12TuneV2_ProbNNghost;
   Double_t        p1_MC12TuneV3_ProbNNe;
   Double_t        p1_MC12TuneV3_ProbNNmu;
   Double_t        p1_MC12TuneV3_ProbNNpi;
   Double_t        p1_MC12TuneV3_ProbNNk;
   Double_t        p1_MC12TuneV3_ProbNNp;
   Double_t        p1_MC12TuneV3_ProbNNghost;
   Double_t        p1_MC12TuneV4_ProbNNe;
   Double_t        p1_MC12TuneV4_ProbNNmu;
   Double_t        p1_MC12TuneV4_ProbNNpi;
   Double_t        p1_MC12TuneV4_ProbNNk;
   Double_t        p1_MC12TuneV4_ProbNNp;
   Double_t        p1_MC12TuneV4_ProbNNghost;
   Double_t        p1_MC15TuneV1_ProbNNe;
   Double_t        p1_MC15TuneV1_ProbNNmu;
   Double_t        p1_MC15TuneV1_ProbNNpi;
   Double_t        p1_MC15TuneV1_ProbNNk;
   Double_t        p1_MC15TuneV1_ProbNNp;
   Double_t        p1_MC15TuneV1_ProbNNghost;
   Double_t        p1_BPVIPCHI2;
   Double_t        p1_ETA;
   Double_t        p1_PHI;
   Double_t        p1_OWNPV_X;
   Double_t        p1_OWNPV_Y;
   Double_t        p1_OWNPV_Z;
   Double_t        p1_OWNPV_XERR;
   Double_t        p1_OWNPV_YERR;
   Double_t        p1_OWNPV_ZERR;
   Double_t        p1_OWNPV_CHI2;
   Int_t           p1_OWNPV_NDOF;
   Float_t         p1_OWNPV_COV_[3][3];
   Double_t        p1_IP_OWNPV;
   Double_t        p1_IPCHI2_OWNPV;
   Double_t        p1_ORIVX_X;
   Double_t        p1_ORIVX_Y;
   Double_t        p1_ORIVX_Z;
   Double_t        p1_ORIVX_XERR;
   Double_t        p1_ORIVX_YERR;
   Double_t        p1_ORIVX_ZERR;
   Double_t        p1_ORIVX_CHI2;
   Int_t           p1_ORIVX_NDOF;
   Float_t         p1_ORIVX_COV_[3][3];
   Double_t        p1_P;
   Double_t        p1_PT;
   Double_t        p1_PE;
   Double_t        p1_PX;
   Double_t        p1_PY;
   Double_t        p1_PZ;
   Double_t        p1_M;
   Int_t           p1_ID;
   Double_t        p1_PIDe;
   Double_t        p1_PIDmu;
   Double_t        p1_PIDK;
   Double_t        p1_PIDp;
   Double_t        p1_PIDd;
   Double_t        p1_ProbNNe;
   Double_t        p1_ProbNNk;
   Double_t        p1_ProbNNp;
   Double_t        p1_ProbNNpi;
   Double_t        p1_ProbNNmu;
   Double_t        p1_ProbNNd;
   Double_t        p1_ProbNNghost;
   Bool_t          p1_hasMuon;
   Bool_t          p1_isMuon;
   Bool_t          p1_hasRich;
   Bool_t          p1_UsedRichAerogel;
   Bool_t          p1_UsedRich1Gas;
   Bool_t          p1_UsedRich2Gas;
   Bool_t          p1_RichAboveElThres;
   Bool_t          p1_RichAboveMuThres;
   Bool_t          p1_RichAbovePiThres;
   Bool_t          p1_RichAboveKaThres;
   Bool_t          p1_RichAbovePrThres;
   Bool_t          p1_hasCalo;
   Bool_t          p1_L0Global_Dec;
   Bool_t          p1_L0Global_TIS;
   Bool_t          p1_L0Global_TOS;
   Bool_t          p1_Hlt1Global_Dec;
   Bool_t          p1_Hlt1Global_TIS;
   Bool_t          p1_Hlt1Global_TOS;
   Bool_t          p1_Hlt1Phys_Dec;
   Bool_t          p1_Hlt1Phys_TIS;
   Bool_t          p1_Hlt1Phys_TOS;
   Bool_t          p1_L0PhotonDecision_Dec;
   Bool_t          p1_L0PhotonDecision_TIS;
   Bool_t          p1_L0PhotonDecision_TOS;
   Bool_t          p1_L0HadronDecision_Dec;
   Bool_t          p1_L0HadronDecision_TIS;
   Bool_t          p1_L0HadronDecision_TOS;
   Bool_t          p1_L0MuonDecision_Dec;
   Bool_t          p1_L0MuonDecision_TIS;
   Bool_t          p1_L0MuonDecision_TOS;
   Bool_t          p1_L0ElectronDecision_Dec;
   Bool_t          p1_L0ElectronDecision_TIS;
   Bool_t          p1_L0ElectronDecision_TOS;
   Bool_t          p1_L0DiMuonDecision_Dec;
   Bool_t          p1_L0DiMuonDecision_TIS;
   Bool_t          p1_L0DiMuonDecision_TOS;
   Bool_t          p1_L0GlobalDecision_Dec;
   Bool_t          p1_L0GlobalDecision_TIS;
   Bool_t          p1_L0GlobalDecision_TOS;
   Bool_t          p1_Hlt1TrackMVADecision_Dec;
   Bool_t          p1_Hlt1TrackMVADecision_TIS;
   Bool_t          p1_Hlt1TrackMVADecision_TOS;
   Bool_t          p1_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          p1_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          p1_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          p1_Hlt1GlobalDecision_Dec;
   Bool_t          p1_Hlt1GlobalDecision_TIS;
   Bool_t          p1_Hlt1GlobalDecision_TOS;
   Double_t        p2_MC12TuneV2_ProbNNe;
   Double_t        p2_MC12TuneV2_ProbNNmu;
   Double_t        p2_MC12TuneV2_ProbNNpi;
   Double_t        p2_MC12TuneV2_ProbNNk;
   Double_t        p2_MC12TuneV2_ProbNNp;
   Double_t        p2_MC12TuneV2_ProbNNghost;
   Double_t        p2_MC12TuneV3_ProbNNe;
   Double_t        p2_MC12TuneV3_ProbNNmu;
   Double_t        p2_MC12TuneV3_ProbNNpi;
   Double_t        p2_MC12TuneV3_ProbNNk;
   Double_t        p2_MC12TuneV3_ProbNNp;
   Double_t        p2_MC12TuneV3_ProbNNghost;
   Double_t        p2_MC12TuneV4_ProbNNe;
   Double_t        p2_MC12TuneV4_ProbNNmu;
   Double_t        p2_MC12TuneV4_ProbNNpi;
   Double_t        p2_MC12TuneV4_ProbNNk;
   Double_t        p2_MC12TuneV4_ProbNNp;
   Double_t        p2_MC12TuneV4_ProbNNghost;
   Double_t        p2_MC15TuneV1_ProbNNe;
   Double_t        p2_MC15TuneV1_ProbNNmu;
   Double_t        p2_MC15TuneV1_ProbNNpi;
   Double_t        p2_MC15TuneV1_ProbNNk;
   Double_t        p2_MC15TuneV1_ProbNNp;
   Double_t        p2_MC15TuneV1_ProbNNghost;
   Double_t        p2_BPVIPCHI2;
   Double_t        p2_ETA;
   Double_t        p2_PHI;
   Double_t        p2_OWNPV_X;
   Double_t        p2_OWNPV_Y;
   Double_t        p2_OWNPV_Z;
   Double_t        p2_OWNPV_XERR;
   Double_t        p2_OWNPV_YERR;
   Double_t        p2_OWNPV_ZERR;
   Double_t        p2_OWNPV_CHI2;
   Int_t           p2_OWNPV_NDOF;
   Float_t         p2_OWNPV_COV_[3][3];
   Double_t        p2_IP_OWNPV;
   Double_t        p2_IPCHI2_OWNPV;
   Double_t        p2_ORIVX_X;
   Double_t        p2_ORIVX_Y;
   Double_t        p2_ORIVX_Z;
   Double_t        p2_ORIVX_XERR;
   Double_t        p2_ORIVX_YERR;
   Double_t        p2_ORIVX_ZERR;
   Double_t        p2_ORIVX_CHI2;
   Int_t           p2_ORIVX_NDOF;
   Float_t         p2_ORIVX_COV_[3][3];
   Double_t        p2_P;
   Double_t        p2_PT;
   Double_t        p2_PE;
   Double_t        p2_PX;
   Double_t        p2_PY;
   Double_t        p2_PZ;
   Double_t        p2_M;
   Int_t           p2_ID;
   Double_t        p2_PIDe;
   Double_t        p2_PIDmu;
   Double_t        p2_PIDK;
   Double_t        p2_PIDp;
   Double_t        p2_PIDd;
   Double_t        p2_ProbNNe;
   Double_t        p2_ProbNNk;
   Double_t        p2_ProbNNp;
   Double_t        p2_ProbNNpi;
   Double_t        p2_ProbNNmu;
   Double_t        p2_ProbNNd;
   Double_t        p2_ProbNNghost;
   Bool_t          p2_hasMuon;
   Bool_t          p2_isMuon;
   Bool_t          p2_hasRich;
   Bool_t          p2_UsedRichAerogel;
   Bool_t          p2_UsedRich1Gas;
   Bool_t          p2_UsedRich2Gas;
   Bool_t          p2_RichAboveElThres;
   Bool_t          p2_RichAboveMuThres;
   Bool_t          p2_RichAbovePiThres;
   Bool_t          p2_RichAboveKaThres;
   Bool_t          p2_RichAbovePrThres;
   Bool_t          p2_hasCalo;
   Bool_t          p2_L0Global_Dec;
   Bool_t          p2_L0Global_TIS;
   Bool_t          p2_L0Global_TOS;
   Bool_t          p2_Hlt1Global_Dec;
   Bool_t          p2_Hlt1Global_TIS;
   Bool_t          p2_Hlt1Global_TOS;
   Bool_t          p2_Hlt1Phys_Dec;
   Bool_t          p2_Hlt1Phys_TIS;
   Bool_t          p2_Hlt1Phys_TOS;
   Bool_t          p2_L0PhotonDecision_Dec;
   Bool_t          p2_L0PhotonDecision_TIS;
   Bool_t          p2_L0PhotonDecision_TOS;
   Bool_t          p2_L0HadronDecision_Dec;
   Bool_t          p2_L0HadronDecision_TIS;
   Bool_t          p2_L0HadronDecision_TOS;
   Bool_t          p2_L0MuonDecision_Dec;
   Bool_t          p2_L0MuonDecision_TIS;
   Bool_t          p2_L0MuonDecision_TOS;
   Bool_t          p2_L0ElectronDecision_Dec;
   Bool_t          p2_L0ElectronDecision_TIS;
   Bool_t          p2_L0ElectronDecision_TOS;
   Bool_t          p2_L0DiMuonDecision_Dec;
   Bool_t          p2_L0DiMuonDecision_TIS;
   Bool_t          p2_L0DiMuonDecision_TOS;
   Bool_t          p2_L0GlobalDecision_Dec;
   Bool_t          p2_L0GlobalDecision_TIS;
   Bool_t          p2_L0GlobalDecision_TOS;
   Bool_t          p2_Hlt1TrackMVADecision_Dec;
   Bool_t          p2_Hlt1TrackMVADecision_TIS;
   Bool_t          p2_Hlt1TrackMVADecision_TOS;
   Bool_t          p2_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          p2_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          p2_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          p2_Hlt1GlobalDecision_Dec;
   Bool_t          p2_Hlt1GlobalDecision_TIS;
   Bool_t          p2_Hlt1GlobalDecision_TOS;
   Double_t        p3_MC12TuneV2_ProbNNe;
   Double_t        p3_MC12TuneV2_ProbNNmu;
   Double_t        p3_MC12TuneV2_ProbNNpi;
   Double_t        p3_MC12TuneV2_ProbNNk;
   Double_t        p3_MC12TuneV2_ProbNNp;
   Double_t        p3_MC12TuneV2_ProbNNghost;
   Double_t        p3_MC12TuneV3_ProbNNe;
   Double_t        p3_MC12TuneV3_ProbNNmu;
   Double_t        p3_MC12TuneV3_ProbNNpi;
   Double_t        p3_MC12TuneV3_ProbNNk;
   Double_t        p3_MC12TuneV3_ProbNNp;
   Double_t        p3_MC12TuneV3_ProbNNghost;
   Double_t        p3_MC12TuneV4_ProbNNe;
   Double_t        p3_MC12TuneV4_ProbNNmu;
   Double_t        p3_MC12TuneV4_ProbNNpi;
   Double_t        p3_MC12TuneV4_ProbNNk;
   Double_t        p3_MC12TuneV4_ProbNNp;
   Double_t        p3_MC12TuneV4_ProbNNghost;
   Double_t        p3_MC15TuneV1_ProbNNe;
   Double_t        p3_MC15TuneV1_ProbNNmu;
   Double_t        p3_MC15TuneV1_ProbNNpi;
   Double_t        p3_MC15TuneV1_ProbNNk;
   Double_t        p3_MC15TuneV1_ProbNNp;
   Double_t        p3_MC15TuneV1_ProbNNghost;
   Double_t        p3_BPVIPCHI2;
   Double_t        p3_ETA;
   Double_t        p3_PHI;
   Double_t        p3_OWNPV_X;
   Double_t        p3_OWNPV_Y;
   Double_t        p3_OWNPV_Z;
   Double_t        p3_OWNPV_XERR;
   Double_t        p3_OWNPV_YERR;
   Double_t        p3_OWNPV_ZERR;
   Double_t        p3_OWNPV_CHI2;
   Int_t           p3_OWNPV_NDOF;
   Float_t         p3_OWNPV_COV_[3][3];
   Double_t        p3_IP_OWNPV;
   Double_t        p3_IPCHI2_OWNPV;
   Double_t        p3_ORIVX_X;
   Double_t        p3_ORIVX_Y;
   Double_t        p3_ORIVX_Z;
   Double_t        p3_ORIVX_XERR;
   Double_t        p3_ORIVX_YERR;
   Double_t        p3_ORIVX_ZERR;
   Double_t        p3_ORIVX_CHI2;
   Int_t           p3_ORIVX_NDOF;
   Float_t         p3_ORIVX_COV_[3][3];
   Double_t        p3_P;
   Double_t        p3_PT;
   Double_t        p3_PE;
   Double_t        p3_PX;
   Double_t        p3_PY;
   Double_t        p3_PZ;
   Double_t        p3_M;
   Int_t           p3_ID;
   Double_t        p3_PIDe;
   Double_t        p3_PIDmu;
   Double_t        p3_PIDK;
   Double_t        p3_PIDp;
   Double_t        p3_PIDd;
   Double_t        p3_ProbNNe;
   Double_t        p3_ProbNNk;
   Double_t        p3_ProbNNp;
   Double_t        p3_ProbNNpi;
   Double_t        p3_ProbNNmu;
   Double_t        p3_ProbNNd;
   Double_t        p3_ProbNNghost;
   Bool_t          p3_hasMuon;
   Bool_t          p3_isMuon;
   Bool_t          p3_hasRich;
   Bool_t          p3_UsedRichAerogel;
   Bool_t          p3_UsedRich1Gas;
   Bool_t          p3_UsedRich2Gas;
   Bool_t          p3_RichAboveElThres;
   Bool_t          p3_RichAboveMuThres;
   Bool_t          p3_RichAbovePiThres;
   Bool_t          p3_RichAboveKaThres;
   Bool_t          p3_RichAbovePrThres;
   Bool_t          p3_hasCalo;
   Bool_t          p3_L0Global_Dec;
   Bool_t          p3_L0Global_TIS;
   Bool_t          p3_L0Global_TOS;
   Bool_t          p3_Hlt1Global_Dec;
   Bool_t          p3_Hlt1Global_TIS;
   Bool_t          p3_Hlt1Global_TOS;
   Bool_t          p3_Hlt1Phys_Dec;
   Bool_t          p3_Hlt1Phys_TIS;
   Bool_t          p3_Hlt1Phys_TOS;
   Bool_t          p3_L0PhotonDecision_Dec;
   Bool_t          p3_L0PhotonDecision_TIS;
   Bool_t          p3_L0PhotonDecision_TOS;
   Bool_t          p3_L0HadronDecision_Dec;
   Bool_t          p3_L0HadronDecision_TIS;
   Bool_t          p3_L0HadronDecision_TOS;
   Bool_t          p3_L0MuonDecision_Dec;
   Bool_t          p3_L0MuonDecision_TIS;
   Bool_t          p3_L0MuonDecision_TOS;
   Bool_t          p3_L0ElectronDecision_Dec;
   Bool_t          p3_L0ElectronDecision_TIS;
   Bool_t          p3_L0ElectronDecision_TOS;
   Bool_t          p3_L0DiMuonDecision_Dec;
   Bool_t          p3_L0DiMuonDecision_TIS;
   Bool_t          p3_L0DiMuonDecision_TOS;
   Bool_t          p3_L0GlobalDecision_Dec;
   Bool_t          p3_L0GlobalDecision_TIS;
   Bool_t          p3_L0GlobalDecision_TOS;
   Bool_t          p3_Hlt1TrackMVADecision_Dec;
   Bool_t          p3_Hlt1TrackMVADecision_TIS;
   Bool_t          p3_Hlt1TrackMVADecision_TOS;
   Bool_t          p3_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          p3_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          p3_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          p3_Hlt1GlobalDecision_Dec;
   Bool_t          p3_Hlt1GlobalDecision_TIS;
   Bool_t          p3_Hlt1GlobalDecision_TOS;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           L0Data_DiMuon_Pt;
   Int_t           L0Data_DiMuonProd_Pt1Pt2;
   Int_t           L0Data_Electron_Et;
   Int_t           L0Data_GlobalPi0_Et;
   Int_t           L0Data_Hadron_Et;
   Int_t           L0Data_LocalPi0_Et;
   Int_t           L0Data_Muon1_Pt;
   Int_t           L0Data_Muon1_Sgn;
   Int_t           L0Data_Muon2_Pt;
   Int_t           L0Data_Muon2_Sgn;
   Int_t           L0Data_Muon3_Pt;
   Int_t           L0Data_Muon3_Sgn;
   Int_t           L0Data_PUHits_Mult;
   Int_t           L0Data_PUPeak1_Cont;
   Int_t           L0Data_PUPeak1_Pos;
   Int_t           L0Data_PUPeak2_Cont;
   Int_t           L0Data_PUPeak2_Pos;
   Int_t           L0Data_Photon_Et;
   Int_t           L0Data_Spd_Mult;
   Int_t           L0Data_Sum_Et;
   Int_t           L0Data_Sum_Et_Next1;
   Int_t           L0Data_Sum_Et_Next2;
   Int_t           L0Data_Sum_Et_Prev1;
   Int_t           L0Data_Sum_Et_Prev2;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;

   // List of branches
   TBranch        *b_D_DTF_CHI2NDOF;   //!
   TBranch        *b_D_DTF_M;   //!
   TBranch        *b_D_DTF_PE;   //!
   TBranch        *b_D_DTF_PX;   //!
   TBranch        *b_D_DTF_PY;   //!
   TBranch        *b_D_DTF_PZ;   //!
   TBranch        *b_D_DTF_VTX_CHI2NDOF;   //!
   TBranch        *b_D_DTF_VTX_M;   //!
   TBranch        *b_D_DTF_VTX_PE;   //!
   TBranch        *b_D_DTF_VTX_PX;   //!
   TBranch        *b_D_DTF_VTX_PY;   //!
   TBranch        *b_D_DTF_VTX_PZ;   //!
   TBranch        *b_D_DTF_VTX_p1_PE;   //!
   TBranch        *b_D_DTF_VTX_p1_PX;   //!
   TBranch        *b_D_DTF_VTX_p1_PY;   //!
   TBranch        *b_D_DTF_VTX_p1_PZ;   //!
   TBranch        *b_D_DTF_VTX_p2_PE;   //!
   TBranch        *b_D_DTF_VTX_p2_PX;   //!
   TBranch        *b_D_DTF_VTX_p2_PY;   //!
   TBranch        *b_D_DTF_VTX_p2_PZ;   //!
   TBranch        *b_D_DTF_VTX_p3_PE;   //!
   TBranch        *b_D_DTF_VTX_p3_PX;   //!
   TBranch        *b_D_DTF_VTX_p3_PY;   //!
   TBranch        *b_D_DTF_VTX_p3_PZ;   //!
   TBranch        *b_D_DTF_p1_PE;   //!
   TBranch        *b_D_DTF_p1_PX;   //!
   TBranch        *b_D_DTF_p1_PY;   //!
   TBranch        *b_D_DTF_p1_PZ;   //!
   TBranch        *b_D_DTF_p2_PE;   //!
   TBranch        *b_D_DTF_p2_PX;   //!
   TBranch        *b_D_DTF_p2_PY;   //!
   TBranch        *b_D_DTF_p2_PZ;   //!
   TBranch        *b_D_DTF_p3_PE;   //!
   TBranch        *b_D_DTF_p3_PX;   //!
   TBranch        *b_D_DTF_p3_PY;   //!
   TBranch        *b_D_DTF_p3_PZ;   //!
   TBranch        *b_D_BPVTRGPOINTING;   //!
   TBranch        *b_D_CONE1ANGLE;   //!
   TBranch        *b_D_CONE1ANGLEDaughter;   //!
   TBranch        *b_D_CONE1MULT;   //!
   TBranch        *b_D_CONE1MULTDaughter;   //!
   TBranch        *b_D_CONE1PTASYM;   //!
   TBranch        *b_D_CONE1PTASYMDaughter;   //!
   TBranch        *b_D_CONE2ANGLE;   //!
   TBranch        *b_D_CONE2MULT;   //!
   TBranch        *b_D_CONE2PTASYM;   //!
   TBranch        *b_D_DOCA;   //!
   TBranch        *b_D_DOCA12;   //!
   TBranch        *b_D_DOCA12_CHI2;   //!
   TBranch        *b_D_DOCA13;   //!
   TBranch        *b_D_DOCA13_CHI2;   //!
   TBranch        *b_D_DOCA23;   //!
   TBranch        *b_D_DOCA23_CHI2;   //!
   TBranch        *b_D_ETA;   //!
   TBranch        *b_D_PHI;   //!
   TBranch        *b_D_ENDVERTEX_X;   //!
   TBranch        *b_D_ENDVERTEX_Y;   //!
   TBranch        *b_D_ENDVERTEX_Z;   //!
   TBranch        *b_D_ENDVERTEX_XERR;   //!
   TBranch        *b_D_ENDVERTEX_YERR;   //!
   TBranch        *b_D_ENDVERTEX_ZERR;   //!
   TBranch        *b_D_ENDVERTEX_CHI2;   //!
   TBranch        *b_D_ENDVERTEX_NDOF;   //!
   TBranch        *b_D_ENDVERTEX_COV_;   //!
   TBranch        *b_D_OWNPV_X;   //!
   TBranch        *b_D_OWNPV_Y;   //!
   TBranch        *b_D_OWNPV_Z;   //!
   TBranch        *b_D_OWNPV_XERR;   //!
   TBranch        *b_D_OWNPV_YERR;   //!
   TBranch        *b_D_OWNPV_ZERR;   //!
   TBranch        *b_D_OWNPV_CHI2;   //!
   TBranch        *b_D_OWNPV_NDOF;   //!
   TBranch        *b_D_OWNPV_COV_;   //!
   TBranch        *b_D_IP_OWNPV;   //!
   TBranch        *b_D_IPCHI2_OWNPV;   //!
   TBranch        *b_D_FD_OWNPV;   //!
   TBranch        *b_D_FDCHI2_OWNPV;   //!
   TBranch        *b_D_DIRA_OWNPV;   //!
   TBranch        *b_D_P;   //!
   TBranch        *b_D_PT;   //!
   TBranch        *b_D_PE;   //!
   TBranch        *b_D_PX;   //!
   TBranch        *b_D_PY;   //!
   TBranch        *b_D_PZ;   //!
   TBranch        *b_D_MM;   //!
   TBranch        *b_D_MMERR;   //!
   TBranch        *b_D_M;   //!
   TBranch        *b_D_ID;   //!
   TBranch        *b_D_TAU;   //!
   TBranch        *b_D_TAUERR;   //!
   TBranch        *b_D_TAUCHI2;   //!
   TBranch        *b_D_L0Global_Dec;   //!
   TBranch        *b_D_L0Global_TIS;   //!
   TBranch        *b_D_L0Global_TOS;   //!
   TBranch        *b_D_Hlt1Global_Dec;   //!
   TBranch        *b_D_Hlt1Global_TIS;   //!
   TBranch        *b_D_Hlt1Global_TOS;   //!
   TBranch        *b_D_Hlt1Phys_Dec;   //!
   TBranch        *b_D_Hlt1Phys_TIS;   //!
   TBranch        *b_D_Hlt1Phys_TOS;   //!
   TBranch        *b_D_L0PhotonDecision_Dec;   //!
   TBranch        *b_D_L0PhotonDecision_TIS;   //!
   TBranch        *b_D_L0PhotonDecision_TOS;   //!
   TBranch        *b_D_L0HadronDecision_Dec;   //!
   TBranch        *b_D_L0HadronDecision_TIS;   //!
   TBranch        *b_D_L0HadronDecision_TOS;   //!
   TBranch        *b_D_L0MuonDecision_Dec;   //!
   TBranch        *b_D_L0MuonDecision_TIS;   //!
   TBranch        *b_D_L0MuonDecision_TOS;   //!
   TBranch        *b_D_L0ElectronDecision_Dec;   //!
   TBranch        *b_D_L0ElectronDecision_TIS;   //!
   TBranch        *b_D_L0ElectronDecision_TOS;   //!
   TBranch        *b_D_L0DiMuonDecision_Dec;   //!
   TBranch        *b_D_L0DiMuonDecision_TIS;   //!
   TBranch        *b_D_L0DiMuonDecision_TOS;   //!
   TBranch        *b_D_L0GlobalDecision_Dec;   //!
   TBranch        *b_D_L0GlobalDecision_TIS;   //!
   TBranch        *b_D_L0GlobalDecision_TOS;   //!
   TBranch        *b_D_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_D_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_D_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_D_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_D_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_D_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_D_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_D_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_D_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_p1_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_p1_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_p1_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_p1_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_p1_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_p1_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_p1_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_p1_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_p1_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_p1_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_p1_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_p1_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_p1_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_p1_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_p1_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_p1_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_p1_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_p1_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_p1_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_p1_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_p1_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_p1_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_p1_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_p1_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_p1_BPVIPCHI2;   //!
   TBranch        *b_p1_ETA;   //!
   TBranch        *b_p1_PHI;   //!
   TBranch        *b_p1_OWNPV_X;   //!
   TBranch        *b_p1_OWNPV_Y;   //!
   TBranch        *b_p1_OWNPV_Z;   //!
   TBranch        *b_p1_OWNPV_XERR;   //!
   TBranch        *b_p1_OWNPV_YERR;   //!
   TBranch        *b_p1_OWNPV_ZERR;   //!
   TBranch        *b_p1_OWNPV_CHI2;   //!
   TBranch        *b_p1_OWNPV_NDOF;   //!
   TBranch        *b_p1_OWNPV_COV_;   //!
   TBranch        *b_p1_IP_OWNPV;   //!
   TBranch        *b_p1_IPCHI2_OWNPV;   //!
   TBranch        *b_p1_ORIVX_X;   //!
   TBranch        *b_p1_ORIVX_Y;   //!
   TBranch        *b_p1_ORIVX_Z;   //!
   TBranch        *b_p1_ORIVX_XERR;   //!
   TBranch        *b_p1_ORIVX_YERR;   //!
   TBranch        *b_p1_ORIVX_ZERR;   //!
   TBranch        *b_p1_ORIVX_CHI2;   //!
   TBranch        *b_p1_ORIVX_NDOF;   //!
   TBranch        *b_p1_ORIVX_COV_;   //!
   TBranch        *b_p1_P;   //!
   TBranch        *b_p1_PT;   //!
   TBranch        *b_p1_PE;   //!
   TBranch        *b_p1_PX;   //!
   TBranch        *b_p1_PY;   //!
   TBranch        *b_p1_PZ;   //!
   TBranch        *b_p1_M;   //!
   TBranch        *b_p1_ID;   //!
   TBranch        *b_p1_PIDe;   //!
   TBranch        *b_p1_PIDmu;   //!
   TBranch        *b_p1_PIDK;   //!
   TBranch        *b_p1_PIDp;   //!
   TBranch        *b_p1_PIDd;   //!
   TBranch        *b_p1_ProbNNe;   //!
   TBranch        *b_p1_ProbNNk;   //!
   TBranch        *b_p1_ProbNNp;   //!
   TBranch        *b_p1_ProbNNpi;   //!
   TBranch        *b_p1_ProbNNmu;   //!
   TBranch        *b_p1_ProbNNd;   //!
   TBranch        *b_p1_ProbNNghost;   //!
   TBranch        *b_p1_hasMuon;   //!
   TBranch        *b_p1_isMuon;   //!
   TBranch        *b_p1_hasRich;   //!
   TBranch        *b_p1_UsedRichAerogel;   //!
   TBranch        *b_p1_UsedRich1Gas;   //!
   TBranch        *b_p1_UsedRich2Gas;   //!
   TBranch        *b_p1_RichAboveElThres;   //!
   TBranch        *b_p1_RichAboveMuThres;   //!
   TBranch        *b_p1_RichAbovePiThres;   //!
   TBranch        *b_p1_RichAboveKaThres;   //!
   TBranch        *b_p1_RichAbovePrThres;   //!
   TBranch        *b_p1_hasCalo;   //!
   TBranch        *b_p1_L0Global_Dec;   //!
   TBranch        *b_p1_L0Global_TIS;   //!
   TBranch        *b_p1_L0Global_TOS;   //!
   TBranch        *b_p1_Hlt1Global_Dec;   //!
   TBranch        *b_p1_Hlt1Global_TIS;   //!
   TBranch        *b_p1_Hlt1Global_TOS;   //!
   TBranch        *b_p1_Hlt1Phys_Dec;   //!
   TBranch        *b_p1_Hlt1Phys_TIS;   //!
   TBranch        *b_p1_Hlt1Phys_TOS;   //!
   TBranch        *b_p1_L0PhotonDecision_Dec;   //!
   TBranch        *b_p1_L0PhotonDecision_TIS;   //!
   TBranch        *b_p1_L0PhotonDecision_TOS;   //!
   TBranch        *b_p1_L0HadronDecision_Dec;   //!
   TBranch        *b_p1_L0HadronDecision_TIS;   //!
   TBranch        *b_p1_L0HadronDecision_TOS;   //!
   TBranch        *b_p1_L0MuonDecision_Dec;   //!
   TBranch        *b_p1_L0MuonDecision_TIS;   //!
   TBranch        *b_p1_L0MuonDecision_TOS;   //!
   TBranch        *b_p1_L0ElectronDecision_Dec;   //!
   TBranch        *b_p1_L0ElectronDecision_TIS;   //!
   TBranch        *b_p1_L0ElectronDecision_TOS;   //!
   TBranch        *b_p1_L0DiMuonDecision_Dec;   //!
   TBranch        *b_p1_L0DiMuonDecision_TIS;   //!
   TBranch        *b_p1_L0DiMuonDecision_TOS;   //!
   TBranch        *b_p1_L0GlobalDecision_Dec;   //!
   TBranch        *b_p1_L0GlobalDecision_TIS;   //!
   TBranch        *b_p1_L0GlobalDecision_TOS;   //!
   TBranch        *b_p1_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_p1_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_p1_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_p1_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_p1_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_p1_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_p1_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_p1_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_p1_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_p2_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_p2_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_p2_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_p2_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_p2_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_p2_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_p2_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_p2_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_p2_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_p2_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_p2_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_p2_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_p2_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_p2_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_p2_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_p2_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_p2_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_p2_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_p2_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_p2_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_p2_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_p2_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_p2_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_p2_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_p2_BPVIPCHI2;   //!
   TBranch        *b_p2_ETA;   //!
   TBranch        *b_p2_PHI;   //!
   TBranch        *b_p2_OWNPV_X;   //!
   TBranch        *b_p2_OWNPV_Y;   //!
   TBranch        *b_p2_OWNPV_Z;   //!
   TBranch        *b_p2_OWNPV_XERR;   //!
   TBranch        *b_p2_OWNPV_YERR;   //!
   TBranch        *b_p2_OWNPV_ZERR;   //!
   TBranch        *b_p2_OWNPV_CHI2;   //!
   TBranch        *b_p2_OWNPV_NDOF;   //!
   TBranch        *b_p2_OWNPV_COV_;   //!
   TBranch        *b_p2_IP_OWNPV;   //!
   TBranch        *b_p2_IPCHI2_OWNPV;   //!
   TBranch        *b_p2_ORIVX_X;   //!
   TBranch        *b_p2_ORIVX_Y;   //!
   TBranch        *b_p2_ORIVX_Z;   //!
   TBranch        *b_p2_ORIVX_XERR;   //!
   TBranch        *b_p2_ORIVX_YERR;   //!
   TBranch        *b_p2_ORIVX_ZERR;   //!
   TBranch        *b_p2_ORIVX_CHI2;   //!
   TBranch        *b_p2_ORIVX_NDOF;   //!
   TBranch        *b_p2_ORIVX_COV_;   //!
   TBranch        *b_p2_P;   //!
   TBranch        *b_p2_PT;   //!
   TBranch        *b_p2_PE;   //!
   TBranch        *b_p2_PX;   //!
   TBranch        *b_p2_PY;   //!
   TBranch        *b_p2_PZ;   //!
   TBranch        *b_p2_M;   //!
   TBranch        *b_p2_ID;   //!
   TBranch        *b_p2_PIDe;   //!
   TBranch        *b_p2_PIDmu;   //!
   TBranch        *b_p2_PIDK;   //!
   TBranch        *b_p2_PIDp;   //!
   TBranch        *b_p2_PIDd;   //!
   TBranch        *b_p2_ProbNNe;   //!
   TBranch        *b_p2_ProbNNk;   //!
   TBranch        *b_p2_ProbNNp;   //!
   TBranch        *b_p2_ProbNNpi;   //!
   TBranch        *b_p2_ProbNNmu;   //!
   TBranch        *b_p2_ProbNNd;   //!
   TBranch        *b_p2_ProbNNghost;   //!
   TBranch        *b_p2_hasMuon;   //!
   TBranch        *b_p2_isMuon;   //!
   TBranch        *b_p2_hasRich;   //!
   TBranch        *b_p2_UsedRichAerogel;   //!
   TBranch        *b_p2_UsedRich1Gas;   //!
   TBranch        *b_p2_UsedRich2Gas;   //!
   TBranch        *b_p2_RichAboveElThres;   //!
   TBranch        *b_p2_RichAboveMuThres;   //!
   TBranch        *b_p2_RichAbovePiThres;   //!
   TBranch        *b_p2_RichAboveKaThres;   //!
   TBranch        *b_p2_RichAbovePrThres;   //!
   TBranch        *b_p2_hasCalo;   //!
   TBranch        *b_p2_L0Global_Dec;   //!
   TBranch        *b_p2_L0Global_TIS;   //!
   TBranch        *b_p2_L0Global_TOS;   //!
   TBranch        *b_p2_Hlt1Global_Dec;   //!
   TBranch        *b_p2_Hlt1Global_TIS;   //!
   TBranch        *b_p2_Hlt1Global_TOS;   //!
   TBranch        *b_p2_Hlt1Phys_Dec;   //!
   TBranch        *b_p2_Hlt1Phys_TIS;   //!
   TBranch        *b_p2_Hlt1Phys_TOS;   //!
   TBranch        *b_p2_L0PhotonDecision_Dec;   //!
   TBranch        *b_p2_L0PhotonDecision_TIS;   //!
   TBranch        *b_p2_L0PhotonDecision_TOS;   //!
   TBranch        *b_p2_L0HadronDecision_Dec;   //!
   TBranch        *b_p2_L0HadronDecision_TIS;   //!
   TBranch        *b_p2_L0HadronDecision_TOS;   //!
   TBranch        *b_p2_L0MuonDecision_Dec;   //!
   TBranch        *b_p2_L0MuonDecision_TIS;   //!
   TBranch        *b_p2_L0MuonDecision_TOS;   //!
   TBranch        *b_p2_L0ElectronDecision_Dec;   //!
   TBranch        *b_p2_L0ElectronDecision_TIS;   //!
   TBranch        *b_p2_L0ElectronDecision_TOS;   //!
   TBranch        *b_p2_L0DiMuonDecision_Dec;   //!
   TBranch        *b_p2_L0DiMuonDecision_TIS;   //!
   TBranch        *b_p2_L0DiMuonDecision_TOS;   //!
   TBranch        *b_p2_L0GlobalDecision_Dec;   //!
   TBranch        *b_p2_L0GlobalDecision_TIS;   //!
   TBranch        *b_p2_L0GlobalDecision_TOS;   //!
   TBranch        *b_p2_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_p2_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_p2_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_p2_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_p2_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_p2_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_p2_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_p2_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_p2_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_p3_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_p3_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_p3_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_p3_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_p3_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_p3_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_p3_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_p3_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_p3_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_p3_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_p3_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_p3_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_p3_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_p3_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_p3_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_p3_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_p3_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_p3_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_p3_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_p3_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_p3_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_p3_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_p3_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_p3_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_p3_BPVIPCHI2;   //!
   TBranch        *b_p3_ETA;   //!
   TBranch        *b_p3_PHI;   //!
   TBranch        *b_p3_OWNPV_X;   //!
   TBranch        *b_p3_OWNPV_Y;   //!
   TBranch        *b_p3_OWNPV_Z;   //!
   TBranch        *b_p3_OWNPV_XERR;   //!
   TBranch        *b_p3_OWNPV_YERR;   //!
   TBranch        *b_p3_OWNPV_ZERR;   //!
   TBranch        *b_p3_OWNPV_CHI2;   //!
   TBranch        *b_p3_OWNPV_NDOF;   //!
   TBranch        *b_p3_OWNPV_COV_;   //!
   TBranch        *b_p3_IP_OWNPV;   //!
   TBranch        *b_p3_IPCHI2_OWNPV;   //!
   TBranch        *b_p3_ORIVX_X;   //!
   TBranch        *b_p3_ORIVX_Y;   //!
   TBranch        *b_p3_ORIVX_Z;   //!
   TBranch        *b_p3_ORIVX_XERR;   //!
   TBranch        *b_p3_ORIVX_YERR;   //!
   TBranch        *b_p3_ORIVX_ZERR;   //!
   TBranch        *b_p3_ORIVX_CHI2;   //!
   TBranch        *b_p3_ORIVX_NDOF;   //!
   TBranch        *b_p3_ORIVX_COV_;   //!
   TBranch        *b_p3_P;   //!
   TBranch        *b_p3_PT;   //!
   TBranch        *b_p3_PE;   //!
   TBranch        *b_p3_PX;   //!
   TBranch        *b_p3_PY;   //!
   TBranch        *b_p3_PZ;   //!
   TBranch        *b_p3_M;   //!
   TBranch        *b_p3_ID;   //!
   TBranch        *b_p3_PIDe;   //!
   TBranch        *b_p3_PIDmu;   //!
   TBranch        *b_p3_PIDK;   //!
   TBranch        *b_p3_PIDp;   //!
   TBranch        *b_p3_PIDd;   //!
   TBranch        *b_p3_ProbNNe;   //!
   TBranch        *b_p3_ProbNNk;   //!
   TBranch        *b_p3_ProbNNp;   //!
   TBranch        *b_p3_ProbNNpi;   //!
   TBranch        *b_p3_ProbNNmu;   //!
   TBranch        *b_p3_ProbNNd;   //!
   TBranch        *b_p3_ProbNNghost;   //!
   TBranch        *b_p3_hasMuon;   //!
   TBranch        *b_p3_isMuon;   //!
   TBranch        *b_p3_hasRich;   //!
   TBranch        *b_p3_UsedRichAerogel;   //!
   TBranch        *b_p3_UsedRich1Gas;   //!
   TBranch        *b_p3_UsedRich2Gas;   //!
   TBranch        *b_p3_RichAboveElThres;   //!
   TBranch        *b_p3_RichAboveMuThres;   //!
   TBranch        *b_p3_RichAbovePiThres;   //!
   TBranch        *b_p3_RichAboveKaThres;   //!
   TBranch        *b_p3_RichAbovePrThres;   //!
   TBranch        *b_p3_hasCalo;   //!
   TBranch        *b_p3_L0Global_Dec;   //!
   TBranch        *b_p3_L0Global_TIS;   //!
   TBranch        *b_p3_L0Global_TOS;   //!
   TBranch        *b_p3_Hlt1Global_Dec;   //!
   TBranch        *b_p3_Hlt1Global_TIS;   //!
   TBranch        *b_p3_Hlt1Global_TOS;   //!
   TBranch        *b_p3_Hlt1Phys_Dec;   //!
   TBranch        *b_p3_Hlt1Phys_TIS;   //!
   TBranch        *b_p3_Hlt1Phys_TOS;   //!
   TBranch        *b_p3_L0PhotonDecision_Dec;   //!
   TBranch        *b_p3_L0PhotonDecision_TIS;   //!
   TBranch        *b_p3_L0PhotonDecision_TOS;   //!
   TBranch        *b_p3_L0HadronDecision_Dec;   //!
   TBranch        *b_p3_L0HadronDecision_TIS;   //!
   TBranch        *b_p3_L0HadronDecision_TOS;   //!
   TBranch        *b_p3_L0MuonDecision_Dec;   //!
   TBranch        *b_p3_L0MuonDecision_TIS;   //!
   TBranch        *b_p3_L0MuonDecision_TOS;   //!
   TBranch        *b_p3_L0ElectronDecision_Dec;   //!
   TBranch        *b_p3_L0ElectronDecision_TIS;   //!
   TBranch        *b_p3_L0ElectronDecision_TOS;   //!
   TBranch        *b_p3_L0DiMuonDecision_Dec;   //!
   TBranch        *b_p3_L0DiMuonDecision_TIS;   //!
   TBranch        *b_p3_L0DiMuonDecision_TOS;   //!
   TBranch        *b_p3_L0GlobalDecision_Dec;   //!
   TBranch        *b_p3_L0GlobalDecision_TIS;   //!
   TBranch        *b_p3_L0GlobalDecision_TOS;   //!
   TBranch        *b_p3_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_p3_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_p3_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_p3_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_p3_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_p3_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_p3_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_p3_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_p3_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_L0Data_DiMuon_Pt;   //!
   TBranch        *b_L0Data_DiMuonProd_Pt1Pt2;   //!
   TBranch        *b_L0Data_Electron_Et;   //!
   TBranch        *b_L0Data_GlobalPi0_Et;   //!
   TBranch        *b_L0Data_Hadron_Et;   //!
   TBranch        *b_L0Data_LocalPi0_Et;   //!
   TBranch        *b_L0Data_Muon1_Pt;   //!
   TBranch        *b_L0Data_Muon1_Sgn;   //!
   TBranch        *b_L0Data_Muon2_Pt;   //!
   TBranch        *b_L0Data_Muon2_Sgn;   //!
   TBranch        *b_L0Data_Muon3_Pt;   //!
   TBranch        *b_L0Data_Muon3_Sgn;   //!
   TBranch        *b_L0Data_PUHits_Mult;   //!
   TBranch        *b_L0Data_PUPeak1_Cont;   //!
   TBranch        *b_L0Data_PUPeak1_Pos;   //!
   TBranch        *b_L0Data_PUPeak2_Cont;   //!
   TBranch        *b_L0Data_PUPeak2_Pos;   //!
   TBranch        *b_L0Data_Photon_Et;   //!
   TBranch        *b_L0Data_Spd_Mult;   //!
   TBranch        *b_L0Data_Sum_Et;   //!
   TBranch        *b_L0Data_Sum_Et_Next1;   //!
   TBranch        *b_L0Data_Sum_Et_Next2;   //!
   TBranch        *b_L0Data_Sum_Et_Prev1;   //!
   TBranch        *b_L0Data_Sum_Et_Prev2;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!

//Include new method with parameters for eos
   std::string m_daughters;
   std::string m_mother;
   std::string m_magnetPolorJob;
   std::string m_ntupleName;
   std::string m_outputExtension;
  ntpmakerRun2(std::string magnetPolorJob, std::string daughters, std::string mother, std::string job, int firstSubJob, int lastSubjob, std::string  m_outputExtension);   
  ntpmakerRun2(std::string magnetPolorJob, std::string daughters, std::string mother, std::string job, int lastSubjob);   
  ntpmakerRun2(std::string magnetPolorJob, std::string daughters, std::string mother);   


   ntpmakerRun2(TTree *tree=0);
   virtual ~ntpmakerRun2();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ntpmakerRun2_cxx
ntpmakerRun2::ntpmakerRun2(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("ntp_KPP/ntp1",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("ntp_KPP/ntp1","");
      chain->Add("KPP_2018_MomScal.root/ntp_KPP/ntp1");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

//Include new method with parameters for eos

ntpmakerRun2::ntpmakerRun2(std::string magnetPolorJob, std::string daughters, std::string mother, std::string job, int firstSubjob, int lastSubjob,  std::string outputExtension )
{
   m_outputExtension = outputExtension;
   m_daughters = daughters;
   m_magnetPolorJob = magnetPolorJob;
   m_mother = mother;
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f) {
         f = new TFile("Memory Directory");
         f->cd("Rint:/");
      }
      TTree *tree = (TTree*)gDirectory->Get("ntp_KKK/ntp5");

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      if (m_daughters == "KPP" && m_mother == "D") m_ntupleName="ntp_"+m_daughters+"/ntp1";
      if (m_daughters == "KKP" && m_mother == "D") m_ntupleName="ntp_"+m_daughters+"/ntp1";
      if (m_daughters == "PPP" && m_mother == "D") m_ntupleName="ntp_"+m_daughters+"/ntp1";
      if (m_daughters == "PPK" && m_mother == "D") m_ntupleName="ntp_KPPos/ntp1";
      if (m_daughters == "KKK" && m_mother == "D") m_ntupleName="ntp_"+m_daughters+"/ntp1";
      if (m_daughters == "PKK" && m_mother == "Ds") m_ntupleName="ntp_KKPos/ntp1";
      if (m_daughters == "KKP" && m_mother == "Ds") m_ntupleName="ntp_Ds"+m_daughters+"/ntp1";
      if (m_daughters == "PPP" && m_mother == "Ds") m_ntupleName="ntp_Ds"+m_daughters+"/ntp1";
      if (m_daughters == "PPK" && m_mother == "Ds") m_ntupleName="ntp_DsKPPos/ntp1";
      if (m_daughters == "KKK" && m_mother == "Ds") m_ntupleName="ntp_Ds"+m_daughters+"/ntp1";
      if (m_daughters == "KPP" && m_mother == "Ds") m_ntupleName="ntp_Ds"+m_daughters+"/ntp1";

//      m_ntupleName="ntp_Ds"+m_daughters+"/DecayTree";
      TChain * chain = new TChain(m_ntupleName.c_str(),"");

      //TString XROOTD_protocol_prefix = "root://castorlhcb.cern.ch//castor/cern.ch/user/t/tostes/";
      //TString XROOTD_protocol_prefix = "root://castorlhcb.cern.ch//castor/cern.ch/user/s/sandra/";
      TString XROOTD_protocol_prefix = "root://eoslhcb.cern.ch//eos/lhcb/user/s/sandra/";

      for (Int_t i = firstSubjob; i <= lastSubjob; i++)
        {
          cout << "Added subjob " << i << endl;
          std::ostringstream oss;
          oss << XROOTD_protocol_prefix << "Job" << job << "/" << i << "/TeslaTuples.root/"+m_ntupleName;
          chain->Add(oss.str().c_str());
        }

      TTree *tree = chain;
#endif // SINGLE_TREE
   Init(tree);
}



ntpmakerRun2::~ntpmakerRun2()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpmakerRun2::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpmakerRun2::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpmakerRun2::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("D_DTF_CHI2NDOF", &D_DTF_CHI2NDOF, &b_D_DTF_CHI2NDOF);
   fChain->SetBranchAddress("D_DTF_M", &D_DTF_M, &b_D_DTF_M);
   fChain->SetBranchAddress("D_DTF_PE", &D_DTF_PE, &b_D_DTF_PE);
   fChain->SetBranchAddress("D_DTF_PX", &D_DTF_PX, &b_D_DTF_PX);
   fChain->SetBranchAddress("D_DTF_PY", &D_DTF_PY, &b_D_DTF_PY);
   fChain->SetBranchAddress("D_DTF_PZ", &D_DTF_PZ, &b_D_DTF_PZ);
   fChain->SetBranchAddress("D_DTF_VTX_CHI2NDOF", &D_DTF_VTX_CHI2NDOF, &b_D_DTF_VTX_CHI2NDOF);
   fChain->SetBranchAddress("D_DTF_VTX_M", &D_DTF_VTX_M, &b_D_DTF_VTX_M);
   fChain->SetBranchAddress("D_DTF_VTX_PE", &D_DTF_VTX_PE, &b_D_DTF_VTX_PE);
   fChain->SetBranchAddress("D_DTF_VTX_PX", &D_DTF_VTX_PX, &b_D_DTF_VTX_PX);
   fChain->SetBranchAddress("D_DTF_VTX_PY", &D_DTF_VTX_PY, &b_D_DTF_VTX_PY);
   fChain->SetBranchAddress("D_DTF_VTX_PZ", &D_DTF_VTX_PZ, &b_D_DTF_VTX_PZ);
   fChain->SetBranchAddress("D_DTF_VTX_p1_PE", &D_DTF_VTX_p1_PE, &b_D_DTF_VTX_p1_PE);
   fChain->SetBranchAddress("D_DTF_VTX_p1_PX", &D_DTF_VTX_p1_PX, &b_D_DTF_VTX_p1_PX);
   fChain->SetBranchAddress("D_DTF_VTX_p1_PY", &D_DTF_VTX_p1_PY, &b_D_DTF_VTX_p1_PY);
   fChain->SetBranchAddress("D_DTF_VTX_p1_PZ", &D_DTF_VTX_p1_PZ, &b_D_DTF_VTX_p1_PZ);
   fChain->SetBranchAddress("D_DTF_VTX_p2_PE", &D_DTF_VTX_p2_PE, &b_D_DTF_VTX_p2_PE);
   fChain->SetBranchAddress("D_DTF_VTX_p2_PX", &D_DTF_VTX_p2_PX, &b_D_DTF_VTX_p2_PX);
   fChain->SetBranchAddress("D_DTF_VTX_p2_PY", &D_DTF_VTX_p2_PY, &b_D_DTF_VTX_p2_PY);
   fChain->SetBranchAddress("D_DTF_VTX_p2_PZ", &D_DTF_VTX_p2_PZ, &b_D_DTF_VTX_p2_PZ);
   fChain->SetBranchAddress("D_DTF_VTX_p3_PE", &D_DTF_VTX_p3_PE, &b_D_DTF_VTX_p3_PE);
   fChain->SetBranchAddress("D_DTF_VTX_p3_PX", &D_DTF_VTX_p3_PX, &b_D_DTF_VTX_p3_PX);
   fChain->SetBranchAddress("D_DTF_VTX_p3_PY", &D_DTF_VTX_p3_PY, &b_D_DTF_VTX_p3_PY);
   fChain->SetBranchAddress("D_DTF_VTX_p3_PZ", &D_DTF_VTX_p3_PZ, &b_D_DTF_VTX_p3_PZ);
   fChain->SetBranchAddress("D_DTF_p1_PE", &D_DTF_p1_PE, &b_D_DTF_p1_PE);
   fChain->SetBranchAddress("D_DTF_p1_PX", &D_DTF_p1_PX, &b_D_DTF_p1_PX);
   fChain->SetBranchAddress("D_DTF_p1_PY", &D_DTF_p1_PY, &b_D_DTF_p1_PY);
   fChain->SetBranchAddress("D_DTF_p1_PZ", &D_DTF_p1_PZ, &b_D_DTF_p1_PZ);
   fChain->SetBranchAddress("D_DTF_p2_PE", &D_DTF_p2_PE, &b_D_DTF_p2_PE);
   fChain->SetBranchAddress("D_DTF_p2_PX", &D_DTF_p2_PX, &b_D_DTF_p2_PX);
   fChain->SetBranchAddress("D_DTF_p2_PY", &D_DTF_p2_PY, &b_D_DTF_p2_PY);
   fChain->SetBranchAddress("D_DTF_p2_PZ", &D_DTF_p2_PZ, &b_D_DTF_p2_PZ);
   fChain->SetBranchAddress("D_DTF_p3_PE", &D_DTF_p3_PE, &b_D_DTF_p3_PE);
   fChain->SetBranchAddress("D_DTF_p3_PX", &D_DTF_p3_PX, &b_D_DTF_p3_PX);
   fChain->SetBranchAddress("D_DTF_p3_PY", &D_DTF_p3_PY, &b_D_DTF_p3_PY);
   fChain->SetBranchAddress("D_DTF_p3_PZ", &D_DTF_p3_PZ, &b_D_DTF_p3_PZ);
   fChain->SetBranchAddress("D_BPVTRGPOINTING", &D_BPVTRGPOINTING, &b_D_BPVTRGPOINTING);
   fChain->SetBranchAddress("D_CONE1ANGLE", &D_CONE1ANGLE, &b_D_CONE1ANGLE);
   fChain->SetBranchAddress("D_CONE1ANGLEDaughter", &D_CONE1ANGLEDaughter, &b_D_CONE1ANGLEDaughter);
   fChain->SetBranchAddress("D_CONE1MULT", &D_CONE1MULT, &b_D_CONE1MULT);
   fChain->SetBranchAddress("D_CONE1MULTDaughter", &D_CONE1MULTDaughter, &b_D_CONE1MULTDaughter);
   fChain->SetBranchAddress("D_CONE1PTASYM", &D_CONE1PTASYM, &b_D_CONE1PTASYM);
   fChain->SetBranchAddress("D_CONE1PTASYMDaughter", &D_CONE1PTASYMDaughter, &b_D_CONE1PTASYMDaughter);
   fChain->SetBranchAddress("D_CONE2ANGLE", &D_CONE2ANGLE, &b_D_CONE2ANGLE);
   fChain->SetBranchAddress("D_CONE2MULT", &D_CONE2MULT, &b_D_CONE2MULT);
   fChain->SetBranchAddress("D_CONE2PTASYM", &D_CONE2PTASYM, &b_D_CONE2PTASYM);
   fChain->SetBranchAddress("D_DOCA", &D_DOCA, &b_D_DOCA);
   fChain->SetBranchAddress("D_DOCA12", &D_DOCA12, &b_D_DOCA12);
   fChain->SetBranchAddress("D_DOCA12_CHI2", &D_DOCA12_CHI2, &b_D_DOCA12_CHI2);
   fChain->SetBranchAddress("D_DOCA13", &D_DOCA13, &b_D_DOCA13);
   fChain->SetBranchAddress("D_DOCA13_CHI2", &D_DOCA13_CHI2, &b_D_DOCA13_CHI2);
   fChain->SetBranchAddress("D_DOCA23", &D_DOCA23, &b_D_DOCA23);
   fChain->SetBranchAddress("D_DOCA23_CHI2", &D_DOCA23_CHI2, &b_D_DOCA23_CHI2);
   fChain->SetBranchAddress("D_ETA", &D_ETA, &b_D_ETA);
   fChain->SetBranchAddress("D_PHI", &D_PHI, &b_D_PHI);
   fChain->SetBranchAddress("D_ENDVERTEX_X", &D_ENDVERTEX_X, &b_D_ENDVERTEX_X);
   fChain->SetBranchAddress("D_ENDVERTEX_Y", &D_ENDVERTEX_Y, &b_D_ENDVERTEX_Y);
   fChain->SetBranchAddress("D_ENDVERTEX_Z", &D_ENDVERTEX_Z, &b_D_ENDVERTEX_Z);
   fChain->SetBranchAddress("D_ENDVERTEX_XERR", &D_ENDVERTEX_XERR, &b_D_ENDVERTEX_XERR);
   fChain->SetBranchAddress("D_ENDVERTEX_YERR", &D_ENDVERTEX_YERR, &b_D_ENDVERTEX_YERR);
   fChain->SetBranchAddress("D_ENDVERTEX_ZERR", &D_ENDVERTEX_ZERR, &b_D_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("D_ENDVERTEX_CHI2", &D_ENDVERTEX_CHI2, &b_D_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("D_ENDVERTEX_NDOF", &D_ENDVERTEX_NDOF, &b_D_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("D_ENDVERTEX_COV_", D_ENDVERTEX_COV_, &b_D_ENDVERTEX_COV_);
   fChain->SetBranchAddress("D_OWNPV_X", &D_OWNPV_X, &b_D_OWNPV_X);
   fChain->SetBranchAddress("D_OWNPV_Y", &D_OWNPV_Y, &b_D_OWNPV_Y);
   fChain->SetBranchAddress("D_OWNPV_Z", &D_OWNPV_Z, &b_D_OWNPV_Z);
   fChain->SetBranchAddress("D_OWNPV_XERR", &D_OWNPV_XERR, &b_D_OWNPV_XERR);
   fChain->SetBranchAddress("D_OWNPV_YERR", &D_OWNPV_YERR, &b_D_OWNPV_YERR);
   fChain->SetBranchAddress("D_OWNPV_ZERR", &D_OWNPV_ZERR, &b_D_OWNPV_ZERR);
   fChain->SetBranchAddress("D_OWNPV_CHI2", &D_OWNPV_CHI2, &b_D_OWNPV_CHI2);
   fChain->SetBranchAddress("D_OWNPV_NDOF", &D_OWNPV_NDOF, &b_D_OWNPV_NDOF);
   fChain->SetBranchAddress("D_OWNPV_COV_", D_OWNPV_COV_, &b_D_OWNPV_COV_);
   fChain->SetBranchAddress("D_IP_OWNPV", &D_IP_OWNPV, &b_D_IP_OWNPV);
   fChain->SetBranchAddress("D_IPCHI2_OWNPV", &D_IPCHI2_OWNPV, &b_D_IPCHI2_OWNPV);
   fChain->SetBranchAddress("D_FD_OWNPV", &D_FD_OWNPV, &b_D_FD_OWNPV);
   fChain->SetBranchAddress("D_FDCHI2_OWNPV", &D_FDCHI2_OWNPV, &b_D_FDCHI2_OWNPV);
   fChain->SetBranchAddress("D_DIRA_OWNPV", &D_DIRA_OWNPV, &b_D_DIRA_OWNPV);
   fChain->SetBranchAddress("D_P", &D_P, &b_D_P);
   fChain->SetBranchAddress("D_PT", &D_PT, &b_D_PT);
   fChain->SetBranchAddress("D_PE", &D_PE, &b_D_PE);
   fChain->SetBranchAddress("D_PX", &D_PX, &b_D_PX);
   fChain->SetBranchAddress("D_PY", &D_PY, &b_D_PY);
   fChain->SetBranchAddress("D_PZ", &D_PZ, &b_D_PZ);
   fChain->SetBranchAddress("D_MM", &D_MM, &b_D_MM);
   fChain->SetBranchAddress("D_MMERR", &D_MMERR, &b_D_MMERR);
   fChain->SetBranchAddress("D_M", &D_M, &b_D_M);
   fChain->SetBranchAddress("D_ID", &D_ID, &b_D_ID);
   fChain->SetBranchAddress("D_TAU", &D_TAU, &b_D_TAU);
   fChain->SetBranchAddress("D_TAUERR", &D_TAUERR, &b_D_TAUERR);
   fChain->SetBranchAddress("D_TAUCHI2", &D_TAUCHI2, &b_D_TAUCHI2);
   fChain->SetBranchAddress("D_L0Global_Dec", &D_L0Global_Dec, &b_D_L0Global_Dec);
   fChain->SetBranchAddress("D_L0Global_TIS", &D_L0Global_TIS, &b_D_L0Global_TIS);
   fChain->SetBranchAddress("D_L0Global_TOS", &D_L0Global_TOS, &b_D_L0Global_TOS);
   fChain->SetBranchAddress("D_Hlt1Global_Dec", &D_Hlt1Global_Dec, &b_D_Hlt1Global_Dec);
   fChain->SetBranchAddress("D_Hlt1Global_TIS", &D_Hlt1Global_TIS, &b_D_Hlt1Global_TIS);
   fChain->SetBranchAddress("D_Hlt1Global_TOS", &D_Hlt1Global_TOS, &b_D_Hlt1Global_TOS);
   fChain->SetBranchAddress("D_Hlt1Phys_Dec", &D_Hlt1Phys_Dec, &b_D_Hlt1Phys_Dec);
   fChain->SetBranchAddress("D_Hlt1Phys_TIS", &D_Hlt1Phys_TIS, &b_D_Hlt1Phys_TIS);
   fChain->SetBranchAddress("D_Hlt1Phys_TOS", &D_Hlt1Phys_TOS, &b_D_Hlt1Phys_TOS);
   fChain->SetBranchAddress("D_L0PhotonDecision_Dec", &D_L0PhotonDecision_Dec, &b_D_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("D_L0PhotonDecision_TIS", &D_L0PhotonDecision_TIS, &b_D_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("D_L0PhotonDecision_TOS", &D_L0PhotonDecision_TOS, &b_D_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("D_L0HadronDecision_Dec", &D_L0HadronDecision_Dec, &b_D_L0HadronDecision_Dec);
   fChain->SetBranchAddress("D_L0HadronDecision_TIS", &D_L0HadronDecision_TIS, &b_D_L0HadronDecision_TIS);
   fChain->SetBranchAddress("D_L0HadronDecision_TOS", &D_L0HadronDecision_TOS, &b_D_L0HadronDecision_TOS);
   fChain->SetBranchAddress("D_L0MuonDecision_Dec", &D_L0MuonDecision_Dec, &b_D_L0MuonDecision_Dec);
   fChain->SetBranchAddress("D_L0MuonDecision_TIS", &D_L0MuonDecision_TIS, &b_D_L0MuonDecision_TIS);
   fChain->SetBranchAddress("D_L0MuonDecision_TOS", &D_L0MuonDecision_TOS, &b_D_L0MuonDecision_TOS);
   fChain->SetBranchAddress("D_L0ElectronDecision_Dec", &D_L0ElectronDecision_Dec, &b_D_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("D_L0ElectronDecision_TIS", &D_L0ElectronDecision_TIS, &b_D_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("D_L0ElectronDecision_TOS", &D_L0ElectronDecision_TOS, &b_D_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("D_L0DiMuonDecision_Dec", &D_L0DiMuonDecision_Dec, &b_D_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("D_L0DiMuonDecision_TIS", &D_L0DiMuonDecision_TIS, &b_D_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("D_L0DiMuonDecision_TOS", &D_L0DiMuonDecision_TOS, &b_D_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("D_L0GlobalDecision_Dec", &D_L0GlobalDecision_Dec, &b_D_L0GlobalDecision_Dec);
   fChain->SetBranchAddress("D_L0GlobalDecision_TIS", &D_L0GlobalDecision_TIS, &b_D_L0GlobalDecision_TIS);
   fChain->SetBranchAddress("D_L0GlobalDecision_TOS", &D_L0GlobalDecision_TOS, &b_D_L0GlobalDecision_TOS);
   fChain->SetBranchAddress("D_Hlt1TrackMVADecision_Dec", &D_Hlt1TrackMVADecision_Dec, &b_D_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("D_Hlt1TrackMVADecision_TIS", &D_Hlt1TrackMVADecision_TIS, &b_D_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("D_Hlt1TrackMVADecision_TOS", &D_Hlt1TrackMVADecision_TOS, &b_D_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("D_Hlt1TwoTrackMVADecision_Dec", &D_Hlt1TwoTrackMVADecision_Dec, &b_D_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("D_Hlt1TwoTrackMVADecision_TIS", &D_Hlt1TwoTrackMVADecision_TIS, &b_D_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("D_Hlt1TwoTrackMVADecision_TOS", &D_Hlt1TwoTrackMVADecision_TOS, &b_D_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("D_Hlt1GlobalDecision_Dec", &D_Hlt1GlobalDecision_Dec, &b_D_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("D_Hlt1GlobalDecision_TIS", &D_Hlt1GlobalDecision_TIS, &b_D_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("D_Hlt1GlobalDecision_TOS", &D_Hlt1GlobalDecision_TOS, &b_D_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("p1_MC12TuneV2_ProbNNe", &p1_MC12TuneV2_ProbNNe, &b_p1_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("p1_MC12TuneV2_ProbNNmu", &p1_MC12TuneV2_ProbNNmu, &b_p1_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("p1_MC12TuneV2_ProbNNpi", &p1_MC12TuneV2_ProbNNpi, &b_p1_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("p1_MC12TuneV2_ProbNNk", &p1_MC12TuneV2_ProbNNk, &b_p1_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("p1_MC12TuneV2_ProbNNp", &p1_MC12TuneV2_ProbNNp, &b_p1_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("p1_MC12TuneV2_ProbNNghost", &p1_MC12TuneV2_ProbNNghost, &b_p1_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("p1_MC12TuneV3_ProbNNe", &p1_MC12TuneV3_ProbNNe, &b_p1_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("p1_MC12TuneV3_ProbNNmu", &p1_MC12TuneV3_ProbNNmu, &b_p1_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("p1_MC12TuneV3_ProbNNpi", &p1_MC12TuneV3_ProbNNpi, &b_p1_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("p1_MC12TuneV3_ProbNNk", &p1_MC12TuneV3_ProbNNk, &b_p1_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("p1_MC12TuneV3_ProbNNp", &p1_MC12TuneV3_ProbNNp, &b_p1_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("p1_MC12TuneV3_ProbNNghost", &p1_MC12TuneV3_ProbNNghost, &b_p1_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("p1_MC12TuneV4_ProbNNe", &p1_MC12TuneV4_ProbNNe, &b_p1_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("p1_MC12TuneV4_ProbNNmu", &p1_MC12TuneV4_ProbNNmu, &b_p1_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("p1_MC12TuneV4_ProbNNpi", &p1_MC12TuneV4_ProbNNpi, &b_p1_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("p1_MC12TuneV4_ProbNNk", &p1_MC12TuneV4_ProbNNk, &b_p1_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("p1_MC12TuneV4_ProbNNp", &p1_MC12TuneV4_ProbNNp, &b_p1_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("p1_MC12TuneV4_ProbNNghost", &p1_MC12TuneV4_ProbNNghost, &b_p1_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("p1_MC15TuneV1_ProbNNe", &p1_MC15TuneV1_ProbNNe, &b_p1_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("p1_MC15TuneV1_ProbNNmu", &p1_MC15TuneV1_ProbNNmu, &b_p1_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("p1_MC15TuneV1_ProbNNpi", &p1_MC15TuneV1_ProbNNpi, &b_p1_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("p1_MC15TuneV1_ProbNNk", &p1_MC15TuneV1_ProbNNk, &b_p1_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("p1_MC15TuneV1_ProbNNp", &p1_MC15TuneV1_ProbNNp, &b_p1_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("p1_MC15TuneV1_ProbNNghost", &p1_MC15TuneV1_ProbNNghost, &b_p1_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("p1_BPVIPCHI2", &p1_BPVIPCHI2, &b_p1_BPVIPCHI2);
   fChain->SetBranchAddress("p1_ETA", &p1_ETA, &b_p1_ETA);
   fChain->SetBranchAddress("p1_PHI", &p1_PHI, &b_p1_PHI);
   fChain->SetBranchAddress("p1_OWNPV_X", &p1_OWNPV_X, &b_p1_OWNPV_X);
   fChain->SetBranchAddress("p1_OWNPV_Y", &p1_OWNPV_Y, &b_p1_OWNPV_Y);
   fChain->SetBranchAddress("p1_OWNPV_Z", &p1_OWNPV_Z, &b_p1_OWNPV_Z);
   fChain->SetBranchAddress("p1_OWNPV_XERR", &p1_OWNPV_XERR, &b_p1_OWNPV_XERR);
   fChain->SetBranchAddress("p1_OWNPV_YERR", &p1_OWNPV_YERR, &b_p1_OWNPV_YERR);
   fChain->SetBranchAddress("p1_OWNPV_ZERR", &p1_OWNPV_ZERR, &b_p1_OWNPV_ZERR);
   fChain->SetBranchAddress("p1_OWNPV_CHI2", &p1_OWNPV_CHI2, &b_p1_OWNPV_CHI2);
   fChain->SetBranchAddress("p1_OWNPV_NDOF", &p1_OWNPV_NDOF, &b_p1_OWNPV_NDOF);
   fChain->SetBranchAddress("p1_OWNPV_COV_", p1_OWNPV_COV_, &b_p1_OWNPV_COV_);
   fChain->SetBranchAddress("p1_IP_OWNPV", &p1_IP_OWNPV, &b_p1_IP_OWNPV);
   fChain->SetBranchAddress("p1_IPCHI2_OWNPV", &p1_IPCHI2_OWNPV, &b_p1_IPCHI2_OWNPV);
   fChain->SetBranchAddress("p1_ORIVX_X", &p1_ORIVX_X, &b_p1_ORIVX_X);
   fChain->SetBranchAddress("p1_ORIVX_Y", &p1_ORIVX_Y, &b_p1_ORIVX_Y);
   fChain->SetBranchAddress("p1_ORIVX_Z", &p1_ORIVX_Z, &b_p1_ORIVX_Z);
   fChain->SetBranchAddress("p1_ORIVX_XERR", &p1_ORIVX_XERR, &b_p1_ORIVX_XERR);
   fChain->SetBranchAddress("p1_ORIVX_YERR", &p1_ORIVX_YERR, &b_p1_ORIVX_YERR);
   fChain->SetBranchAddress("p1_ORIVX_ZERR", &p1_ORIVX_ZERR, &b_p1_ORIVX_ZERR);
   fChain->SetBranchAddress("p1_ORIVX_CHI2", &p1_ORIVX_CHI2, &b_p1_ORIVX_CHI2);
   fChain->SetBranchAddress("p1_ORIVX_NDOF", &p1_ORIVX_NDOF, &b_p1_ORIVX_NDOF);
   fChain->SetBranchAddress("p1_ORIVX_COV_", p1_ORIVX_COV_, &b_p1_ORIVX_COV_);
   fChain->SetBranchAddress("p1_P", &p1_P, &b_p1_P);
   fChain->SetBranchAddress("p1_PT", &p1_PT, &b_p1_PT);
   fChain->SetBranchAddress("p1_PE", &p1_PE, &b_p1_PE);
   fChain->SetBranchAddress("p1_PX", &p1_PX, &b_p1_PX);
   fChain->SetBranchAddress("p1_PY", &p1_PY, &b_p1_PY);
   fChain->SetBranchAddress("p1_PZ", &p1_PZ, &b_p1_PZ);
   fChain->SetBranchAddress("p1_M", &p1_M, &b_p1_M);
   fChain->SetBranchAddress("p1_ID", &p1_ID, &b_p1_ID);
   fChain->SetBranchAddress("p1_PIDe", &p1_PIDe, &b_p1_PIDe);
   fChain->SetBranchAddress("p1_PIDmu", &p1_PIDmu, &b_p1_PIDmu);
   fChain->SetBranchAddress("p1_PIDK", &p1_PIDK, &b_p1_PIDK);
   fChain->SetBranchAddress("p1_PIDp", &p1_PIDp, &b_p1_PIDp);
   fChain->SetBranchAddress("p1_PIDd", &p1_PIDd, &b_p1_PIDd);
   fChain->SetBranchAddress("p1_ProbNNe", &p1_ProbNNe, &b_p1_ProbNNe);
   fChain->SetBranchAddress("p1_ProbNNk", &p1_ProbNNk, &b_p1_ProbNNk);
   fChain->SetBranchAddress("p1_ProbNNp", &p1_ProbNNp, &b_p1_ProbNNp);
   fChain->SetBranchAddress("p1_ProbNNpi", &p1_ProbNNpi, &b_p1_ProbNNpi);
   fChain->SetBranchAddress("p1_ProbNNmu", &p1_ProbNNmu, &b_p1_ProbNNmu);
   fChain->SetBranchAddress("p1_ProbNNd", &p1_ProbNNd, &b_p1_ProbNNd);
   fChain->SetBranchAddress("p1_ProbNNghost", &p1_ProbNNghost, &b_p1_ProbNNghost);
   fChain->SetBranchAddress("p1_hasMuon", &p1_hasMuon, &b_p1_hasMuon);
   fChain->SetBranchAddress("p1_isMuon", &p1_isMuon, &b_p1_isMuon);
   fChain->SetBranchAddress("p1_hasRich", &p1_hasRich, &b_p1_hasRich);
   fChain->SetBranchAddress("p1_UsedRichAerogel", &p1_UsedRichAerogel, &b_p1_UsedRichAerogel);
   fChain->SetBranchAddress("p1_UsedRich1Gas", &p1_UsedRich1Gas, &b_p1_UsedRich1Gas);
   fChain->SetBranchAddress("p1_UsedRich2Gas", &p1_UsedRich2Gas, &b_p1_UsedRich2Gas);
   fChain->SetBranchAddress("p1_RichAboveElThres", &p1_RichAboveElThres, &b_p1_RichAboveElThres);
   fChain->SetBranchAddress("p1_RichAboveMuThres", &p1_RichAboveMuThres, &b_p1_RichAboveMuThres);
   fChain->SetBranchAddress("p1_RichAbovePiThres", &p1_RichAbovePiThres, &b_p1_RichAbovePiThres);
   fChain->SetBranchAddress("p1_RichAboveKaThres", &p1_RichAboveKaThres, &b_p1_RichAboveKaThres);
   fChain->SetBranchAddress("p1_RichAbovePrThres", &p1_RichAbovePrThres, &b_p1_RichAbovePrThres);
   fChain->SetBranchAddress("p1_hasCalo", &p1_hasCalo, &b_p1_hasCalo);
   fChain->SetBranchAddress("p1_L0Global_Dec", &p1_L0Global_Dec, &b_p1_L0Global_Dec);
   fChain->SetBranchAddress("p1_L0Global_TIS", &p1_L0Global_TIS, &b_p1_L0Global_TIS);
   fChain->SetBranchAddress("p1_L0Global_TOS", &p1_L0Global_TOS, &b_p1_L0Global_TOS);
   fChain->SetBranchAddress("p1_Hlt1Global_Dec", &p1_Hlt1Global_Dec, &b_p1_Hlt1Global_Dec);
   fChain->SetBranchAddress("p1_Hlt1Global_TIS", &p1_Hlt1Global_TIS, &b_p1_Hlt1Global_TIS);
   fChain->SetBranchAddress("p1_Hlt1Global_TOS", &p1_Hlt1Global_TOS, &b_p1_Hlt1Global_TOS);
   fChain->SetBranchAddress("p1_Hlt1Phys_Dec", &p1_Hlt1Phys_Dec, &b_p1_Hlt1Phys_Dec);
   fChain->SetBranchAddress("p1_Hlt1Phys_TIS", &p1_Hlt1Phys_TIS, &b_p1_Hlt1Phys_TIS);
   fChain->SetBranchAddress("p1_Hlt1Phys_TOS", &p1_Hlt1Phys_TOS, &b_p1_Hlt1Phys_TOS);
   fChain->SetBranchAddress("p1_L0PhotonDecision_Dec", &p1_L0PhotonDecision_Dec, &b_p1_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("p1_L0PhotonDecision_TIS", &p1_L0PhotonDecision_TIS, &b_p1_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("p1_L0PhotonDecision_TOS", &p1_L0PhotonDecision_TOS, &b_p1_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("p1_L0HadronDecision_Dec", &p1_L0HadronDecision_Dec, &b_p1_L0HadronDecision_Dec);
   fChain->SetBranchAddress("p1_L0HadronDecision_TIS", &p1_L0HadronDecision_TIS, &b_p1_L0HadronDecision_TIS);
   fChain->SetBranchAddress("p1_L0HadronDecision_TOS", &p1_L0HadronDecision_TOS, &b_p1_L0HadronDecision_TOS);
   fChain->SetBranchAddress("p1_L0MuonDecision_Dec", &p1_L0MuonDecision_Dec, &b_p1_L0MuonDecision_Dec);
   fChain->SetBranchAddress("p1_L0MuonDecision_TIS", &p1_L0MuonDecision_TIS, &b_p1_L0MuonDecision_TIS);
   fChain->SetBranchAddress("p1_L0MuonDecision_TOS", &p1_L0MuonDecision_TOS, &b_p1_L0MuonDecision_TOS);
   fChain->SetBranchAddress("p1_L0ElectronDecision_Dec", &p1_L0ElectronDecision_Dec, &b_p1_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("p1_L0ElectronDecision_TIS", &p1_L0ElectronDecision_TIS, &b_p1_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("p1_L0ElectronDecision_TOS", &p1_L0ElectronDecision_TOS, &b_p1_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("p1_L0DiMuonDecision_Dec", &p1_L0DiMuonDecision_Dec, &b_p1_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("p1_L0DiMuonDecision_TIS", &p1_L0DiMuonDecision_TIS, &b_p1_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("p1_L0DiMuonDecision_TOS", &p1_L0DiMuonDecision_TOS, &b_p1_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("p1_L0GlobalDecision_Dec", &p1_L0GlobalDecision_Dec, &b_p1_L0GlobalDecision_Dec);
   fChain->SetBranchAddress("p1_L0GlobalDecision_TIS", &p1_L0GlobalDecision_TIS, &b_p1_L0GlobalDecision_TIS);
   fChain->SetBranchAddress("p1_L0GlobalDecision_TOS", &p1_L0GlobalDecision_TOS, &b_p1_L0GlobalDecision_TOS);
   fChain->SetBranchAddress("p1_Hlt1TrackMVADecision_Dec", &p1_Hlt1TrackMVADecision_Dec, &b_p1_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("p1_Hlt1TrackMVADecision_TIS", &p1_Hlt1TrackMVADecision_TIS, &b_p1_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("p1_Hlt1TrackMVADecision_TOS", &p1_Hlt1TrackMVADecision_TOS, &b_p1_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("p1_Hlt1TwoTrackMVADecision_Dec", &p1_Hlt1TwoTrackMVADecision_Dec, &b_p1_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("p1_Hlt1TwoTrackMVADecision_TIS", &p1_Hlt1TwoTrackMVADecision_TIS, &b_p1_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("p1_Hlt1TwoTrackMVADecision_TOS", &p1_Hlt1TwoTrackMVADecision_TOS, &b_p1_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("p1_Hlt1GlobalDecision_Dec", &p1_Hlt1GlobalDecision_Dec, &b_p1_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("p1_Hlt1GlobalDecision_TIS", &p1_Hlt1GlobalDecision_TIS, &b_p1_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("p1_Hlt1GlobalDecision_TOS", &p1_Hlt1GlobalDecision_TOS, &b_p1_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("p2_MC12TuneV2_ProbNNe", &p2_MC12TuneV2_ProbNNe, &b_p2_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("p2_MC12TuneV2_ProbNNmu", &p2_MC12TuneV2_ProbNNmu, &b_p2_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("p2_MC12TuneV2_ProbNNpi", &p2_MC12TuneV2_ProbNNpi, &b_p2_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("p2_MC12TuneV2_ProbNNk", &p2_MC12TuneV2_ProbNNk, &b_p2_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("p2_MC12TuneV2_ProbNNp", &p2_MC12TuneV2_ProbNNp, &b_p2_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("p2_MC12TuneV2_ProbNNghost", &p2_MC12TuneV2_ProbNNghost, &b_p2_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("p2_MC12TuneV3_ProbNNe", &p2_MC12TuneV3_ProbNNe, &b_p2_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("p2_MC12TuneV3_ProbNNmu", &p2_MC12TuneV3_ProbNNmu, &b_p2_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("p2_MC12TuneV3_ProbNNpi", &p2_MC12TuneV3_ProbNNpi, &b_p2_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("p2_MC12TuneV3_ProbNNk", &p2_MC12TuneV3_ProbNNk, &b_p2_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("p2_MC12TuneV3_ProbNNp", &p2_MC12TuneV3_ProbNNp, &b_p2_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("p2_MC12TuneV3_ProbNNghost", &p2_MC12TuneV3_ProbNNghost, &b_p2_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("p2_MC12TuneV4_ProbNNe", &p2_MC12TuneV4_ProbNNe, &b_p2_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("p2_MC12TuneV4_ProbNNmu", &p2_MC12TuneV4_ProbNNmu, &b_p2_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("p2_MC12TuneV4_ProbNNpi", &p2_MC12TuneV4_ProbNNpi, &b_p2_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("p2_MC12TuneV4_ProbNNk", &p2_MC12TuneV4_ProbNNk, &b_p2_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("p2_MC12TuneV4_ProbNNp", &p2_MC12TuneV4_ProbNNp, &b_p2_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("p2_MC12TuneV4_ProbNNghost", &p2_MC12TuneV4_ProbNNghost, &b_p2_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("p2_MC15TuneV1_ProbNNe", &p2_MC15TuneV1_ProbNNe, &b_p2_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("p2_MC15TuneV1_ProbNNmu", &p2_MC15TuneV1_ProbNNmu, &b_p2_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("p2_MC15TuneV1_ProbNNpi", &p2_MC15TuneV1_ProbNNpi, &b_p2_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("p2_MC15TuneV1_ProbNNk", &p2_MC15TuneV1_ProbNNk, &b_p2_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("p2_MC15TuneV1_ProbNNp", &p2_MC15TuneV1_ProbNNp, &b_p2_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("p2_MC15TuneV1_ProbNNghost", &p2_MC15TuneV1_ProbNNghost, &b_p2_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("p2_BPVIPCHI2", &p2_BPVIPCHI2, &b_p2_BPVIPCHI2);
   fChain->SetBranchAddress("p2_ETA", &p2_ETA, &b_p2_ETA);
   fChain->SetBranchAddress("p2_PHI", &p2_PHI, &b_p2_PHI);
   fChain->SetBranchAddress("p2_OWNPV_X", &p2_OWNPV_X, &b_p2_OWNPV_X);
   fChain->SetBranchAddress("p2_OWNPV_Y", &p2_OWNPV_Y, &b_p2_OWNPV_Y);
   fChain->SetBranchAddress("p2_OWNPV_Z", &p2_OWNPV_Z, &b_p2_OWNPV_Z);
   fChain->SetBranchAddress("p2_OWNPV_XERR", &p2_OWNPV_XERR, &b_p2_OWNPV_XERR);
   fChain->SetBranchAddress("p2_OWNPV_YERR", &p2_OWNPV_YERR, &b_p2_OWNPV_YERR);
   fChain->SetBranchAddress("p2_OWNPV_ZERR", &p2_OWNPV_ZERR, &b_p2_OWNPV_ZERR);
   fChain->SetBranchAddress("p2_OWNPV_CHI2", &p2_OWNPV_CHI2, &b_p2_OWNPV_CHI2);
   fChain->SetBranchAddress("p2_OWNPV_NDOF", &p2_OWNPV_NDOF, &b_p2_OWNPV_NDOF);
   fChain->SetBranchAddress("p2_OWNPV_COV_", p2_OWNPV_COV_, &b_p2_OWNPV_COV_);
   fChain->SetBranchAddress("p2_IP_OWNPV", &p2_IP_OWNPV, &b_p2_IP_OWNPV);
   fChain->SetBranchAddress("p2_IPCHI2_OWNPV", &p2_IPCHI2_OWNPV, &b_p2_IPCHI2_OWNPV);
   fChain->SetBranchAddress("p2_ORIVX_X", &p2_ORIVX_X, &b_p2_ORIVX_X);
   fChain->SetBranchAddress("p2_ORIVX_Y", &p2_ORIVX_Y, &b_p2_ORIVX_Y);
   fChain->SetBranchAddress("p2_ORIVX_Z", &p2_ORIVX_Z, &b_p2_ORIVX_Z);
   fChain->SetBranchAddress("p2_ORIVX_XERR", &p2_ORIVX_XERR, &b_p2_ORIVX_XERR);
   fChain->SetBranchAddress("p2_ORIVX_YERR", &p2_ORIVX_YERR, &b_p2_ORIVX_YERR);
   fChain->SetBranchAddress("p2_ORIVX_ZERR", &p2_ORIVX_ZERR, &b_p2_ORIVX_ZERR);
   fChain->SetBranchAddress("p2_ORIVX_CHI2", &p2_ORIVX_CHI2, &b_p2_ORIVX_CHI2);
   fChain->SetBranchAddress("p2_ORIVX_NDOF", &p2_ORIVX_NDOF, &b_p2_ORIVX_NDOF);
   fChain->SetBranchAddress("p2_ORIVX_COV_", p2_ORIVX_COV_, &b_p2_ORIVX_COV_);
   fChain->SetBranchAddress("p2_P", &p2_P, &b_p2_P);
   fChain->SetBranchAddress("p2_PT", &p2_PT, &b_p2_PT);
   fChain->SetBranchAddress("p2_PE", &p2_PE, &b_p2_PE);
   fChain->SetBranchAddress("p2_PX", &p2_PX, &b_p2_PX);
   fChain->SetBranchAddress("p2_PY", &p2_PY, &b_p2_PY);
   fChain->SetBranchAddress("p2_PZ", &p2_PZ, &b_p2_PZ);
   fChain->SetBranchAddress("p2_M", &p2_M, &b_p2_M);
   fChain->SetBranchAddress("p2_ID", &p2_ID, &b_p2_ID);
   fChain->SetBranchAddress("p2_PIDe", &p2_PIDe, &b_p2_PIDe);
   fChain->SetBranchAddress("p2_PIDmu", &p2_PIDmu, &b_p2_PIDmu);
   fChain->SetBranchAddress("p2_PIDK", &p2_PIDK, &b_p2_PIDK);
   fChain->SetBranchAddress("p2_PIDp", &p2_PIDp, &b_p2_PIDp);
   fChain->SetBranchAddress("p2_PIDd", &p2_PIDd, &b_p2_PIDd);
   fChain->SetBranchAddress("p2_ProbNNe", &p2_ProbNNe, &b_p2_ProbNNe);
   fChain->SetBranchAddress("p2_ProbNNk", &p2_ProbNNk, &b_p2_ProbNNk);
   fChain->SetBranchAddress("p2_ProbNNp", &p2_ProbNNp, &b_p2_ProbNNp);
   fChain->SetBranchAddress("p2_ProbNNpi", &p2_ProbNNpi, &b_p2_ProbNNpi);
   fChain->SetBranchAddress("p2_ProbNNmu", &p2_ProbNNmu, &b_p2_ProbNNmu);
   fChain->SetBranchAddress("p2_ProbNNd", &p2_ProbNNd, &b_p2_ProbNNd);
   fChain->SetBranchAddress("p2_ProbNNghost", &p2_ProbNNghost, &b_p2_ProbNNghost);
   fChain->SetBranchAddress("p2_hasMuon", &p2_hasMuon, &b_p2_hasMuon);
   fChain->SetBranchAddress("p2_isMuon", &p2_isMuon, &b_p2_isMuon);
   fChain->SetBranchAddress("p2_hasRich", &p2_hasRich, &b_p2_hasRich);
   fChain->SetBranchAddress("p2_UsedRichAerogel", &p2_UsedRichAerogel, &b_p2_UsedRichAerogel);
   fChain->SetBranchAddress("p2_UsedRich1Gas", &p2_UsedRich1Gas, &b_p2_UsedRich1Gas);
   fChain->SetBranchAddress("p2_UsedRich2Gas", &p2_UsedRich2Gas, &b_p2_UsedRich2Gas);
   fChain->SetBranchAddress("p2_RichAboveElThres", &p2_RichAboveElThres, &b_p2_RichAboveElThres);
   fChain->SetBranchAddress("p2_RichAboveMuThres", &p2_RichAboveMuThres, &b_p2_RichAboveMuThres);
   fChain->SetBranchAddress("p2_RichAbovePiThres", &p2_RichAbovePiThres, &b_p2_RichAbovePiThres);
   fChain->SetBranchAddress("p2_RichAboveKaThres", &p2_RichAboveKaThres, &b_p2_RichAboveKaThres);
   fChain->SetBranchAddress("p2_RichAbovePrThres", &p2_RichAbovePrThres, &b_p2_RichAbovePrThres);
   fChain->SetBranchAddress("p2_hasCalo", &p2_hasCalo, &b_p2_hasCalo);
   fChain->SetBranchAddress("p2_L0Global_Dec", &p2_L0Global_Dec, &b_p2_L0Global_Dec);
   fChain->SetBranchAddress("p2_L0Global_TIS", &p2_L0Global_TIS, &b_p2_L0Global_TIS);
   fChain->SetBranchAddress("p2_L0Global_TOS", &p2_L0Global_TOS, &b_p2_L0Global_TOS);
   fChain->SetBranchAddress("p2_Hlt1Global_Dec", &p2_Hlt1Global_Dec, &b_p2_Hlt1Global_Dec);
   fChain->SetBranchAddress("p2_Hlt1Global_TIS", &p2_Hlt1Global_TIS, &b_p2_Hlt1Global_TIS);
   fChain->SetBranchAddress("p2_Hlt1Global_TOS", &p2_Hlt1Global_TOS, &b_p2_Hlt1Global_TOS);
   fChain->SetBranchAddress("p2_Hlt1Phys_Dec", &p2_Hlt1Phys_Dec, &b_p2_Hlt1Phys_Dec);
   fChain->SetBranchAddress("p2_Hlt1Phys_TIS", &p2_Hlt1Phys_TIS, &b_p2_Hlt1Phys_TIS);
   fChain->SetBranchAddress("p2_Hlt1Phys_TOS", &p2_Hlt1Phys_TOS, &b_p2_Hlt1Phys_TOS);
   fChain->SetBranchAddress("p2_L0PhotonDecision_Dec", &p2_L0PhotonDecision_Dec, &b_p2_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("p2_L0PhotonDecision_TIS", &p2_L0PhotonDecision_TIS, &b_p2_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("p2_L0PhotonDecision_TOS", &p2_L0PhotonDecision_TOS, &b_p2_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("p2_L0HadronDecision_Dec", &p2_L0HadronDecision_Dec, &b_p2_L0HadronDecision_Dec);
   fChain->SetBranchAddress("p2_L0HadronDecision_TIS", &p2_L0HadronDecision_TIS, &b_p2_L0HadronDecision_TIS);
   fChain->SetBranchAddress("p2_L0HadronDecision_TOS", &p2_L0HadronDecision_TOS, &b_p2_L0HadronDecision_TOS);
   fChain->SetBranchAddress("p2_L0MuonDecision_Dec", &p2_L0MuonDecision_Dec, &b_p2_L0MuonDecision_Dec);
   fChain->SetBranchAddress("p2_L0MuonDecision_TIS", &p2_L0MuonDecision_TIS, &b_p2_L0MuonDecision_TIS);
   fChain->SetBranchAddress("p2_L0MuonDecision_TOS", &p2_L0MuonDecision_TOS, &b_p2_L0MuonDecision_TOS);
   fChain->SetBranchAddress("p2_L0ElectronDecision_Dec", &p2_L0ElectronDecision_Dec, &b_p2_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("p2_L0ElectronDecision_TIS", &p2_L0ElectronDecision_TIS, &b_p2_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("p2_L0ElectronDecision_TOS", &p2_L0ElectronDecision_TOS, &b_p2_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("p2_L0DiMuonDecision_Dec", &p2_L0DiMuonDecision_Dec, &b_p2_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("p2_L0DiMuonDecision_TIS", &p2_L0DiMuonDecision_TIS, &b_p2_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("p2_L0DiMuonDecision_TOS", &p2_L0DiMuonDecision_TOS, &b_p2_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("p2_L0GlobalDecision_Dec", &p2_L0GlobalDecision_Dec, &b_p2_L0GlobalDecision_Dec);
   fChain->SetBranchAddress("p2_L0GlobalDecision_TIS", &p2_L0GlobalDecision_TIS, &b_p2_L0GlobalDecision_TIS);
   fChain->SetBranchAddress("p2_L0GlobalDecision_TOS", &p2_L0GlobalDecision_TOS, &b_p2_L0GlobalDecision_TOS);
   fChain->SetBranchAddress("p2_Hlt1TrackMVADecision_Dec", &p2_Hlt1TrackMVADecision_Dec, &b_p2_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("p2_Hlt1TrackMVADecision_TIS", &p2_Hlt1TrackMVADecision_TIS, &b_p2_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("p2_Hlt1TrackMVADecision_TOS", &p2_Hlt1TrackMVADecision_TOS, &b_p2_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("p2_Hlt1TwoTrackMVADecision_Dec", &p2_Hlt1TwoTrackMVADecision_Dec, &b_p2_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("p2_Hlt1TwoTrackMVADecision_TIS", &p2_Hlt1TwoTrackMVADecision_TIS, &b_p2_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("p2_Hlt1TwoTrackMVADecision_TOS", &p2_Hlt1TwoTrackMVADecision_TOS, &b_p2_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("p2_Hlt1GlobalDecision_Dec", &p2_Hlt1GlobalDecision_Dec, &b_p2_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("p2_Hlt1GlobalDecision_TIS", &p2_Hlt1GlobalDecision_TIS, &b_p2_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("p2_Hlt1GlobalDecision_TOS", &p2_Hlt1GlobalDecision_TOS, &b_p2_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("p3_MC12TuneV2_ProbNNe", &p3_MC12TuneV2_ProbNNe, &b_p3_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("p3_MC12TuneV2_ProbNNmu", &p3_MC12TuneV2_ProbNNmu, &b_p3_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("p3_MC12TuneV2_ProbNNpi", &p3_MC12TuneV2_ProbNNpi, &b_p3_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("p3_MC12TuneV2_ProbNNk", &p3_MC12TuneV2_ProbNNk, &b_p3_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("p3_MC12TuneV2_ProbNNp", &p3_MC12TuneV2_ProbNNp, &b_p3_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("p3_MC12TuneV2_ProbNNghost", &p3_MC12TuneV2_ProbNNghost, &b_p3_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("p3_MC12TuneV3_ProbNNe", &p3_MC12TuneV3_ProbNNe, &b_p3_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("p3_MC12TuneV3_ProbNNmu", &p3_MC12TuneV3_ProbNNmu, &b_p3_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("p3_MC12TuneV3_ProbNNpi", &p3_MC12TuneV3_ProbNNpi, &b_p3_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("p3_MC12TuneV3_ProbNNk", &p3_MC12TuneV3_ProbNNk, &b_p3_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("p3_MC12TuneV3_ProbNNp", &p3_MC12TuneV3_ProbNNp, &b_p3_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("p3_MC12TuneV3_ProbNNghost", &p3_MC12TuneV3_ProbNNghost, &b_p3_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("p3_MC12TuneV4_ProbNNe", &p3_MC12TuneV4_ProbNNe, &b_p3_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("p3_MC12TuneV4_ProbNNmu", &p3_MC12TuneV4_ProbNNmu, &b_p3_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("p3_MC12TuneV4_ProbNNpi", &p3_MC12TuneV4_ProbNNpi, &b_p3_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("p3_MC12TuneV4_ProbNNk", &p3_MC12TuneV4_ProbNNk, &b_p3_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("p3_MC12TuneV4_ProbNNp", &p3_MC12TuneV4_ProbNNp, &b_p3_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("p3_MC12TuneV4_ProbNNghost", &p3_MC12TuneV4_ProbNNghost, &b_p3_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("p3_MC15TuneV1_ProbNNe", &p3_MC15TuneV1_ProbNNe, &b_p3_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("p3_MC15TuneV1_ProbNNmu", &p3_MC15TuneV1_ProbNNmu, &b_p3_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("p3_MC15TuneV1_ProbNNpi", &p3_MC15TuneV1_ProbNNpi, &b_p3_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("p3_MC15TuneV1_ProbNNk", &p3_MC15TuneV1_ProbNNk, &b_p3_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("p3_MC15TuneV1_ProbNNp", &p3_MC15TuneV1_ProbNNp, &b_p3_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("p3_MC15TuneV1_ProbNNghost", &p3_MC15TuneV1_ProbNNghost, &b_p3_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("p3_BPVIPCHI2", &p3_BPVIPCHI2, &b_p3_BPVIPCHI2);
   fChain->SetBranchAddress("p3_ETA", &p3_ETA, &b_p3_ETA);
   fChain->SetBranchAddress("p3_PHI", &p3_PHI, &b_p3_PHI);
   fChain->SetBranchAddress("p3_OWNPV_X", &p3_OWNPV_X, &b_p3_OWNPV_X);
   fChain->SetBranchAddress("p3_OWNPV_Y", &p3_OWNPV_Y, &b_p3_OWNPV_Y);
   fChain->SetBranchAddress("p3_OWNPV_Z", &p3_OWNPV_Z, &b_p3_OWNPV_Z);
   fChain->SetBranchAddress("p3_OWNPV_XERR", &p3_OWNPV_XERR, &b_p3_OWNPV_XERR);
   fChain->SetBranchAddress("p3_OWNPV_YERR", &p3_OWNPV_YERR, &b_p3_OWNPV_YERR);
   fChain->SetBranchAddress("p3_OWNPV_ZERR", &p3_OWNPV_ZERR, &b_p3_OWNPV_ZERR);
   fChain->SetBranchAddress("p3_OWNPV_CHI2", &p3_OWNPV_CHI2, &b_p3_OWNPV_CHI2);
   fChain->SetBranchAddress("p3_OWNPV_NDOF", &p3_OWNPV_NDOF, &b_p3_OWNPV_NDOF);
   fChain->SetBranchAddress("p3_OWNPV_COV_", p3_OWNPV_COV_, &b_p3_OWNPV_COV_);
   fChain->SetBranchAddress("p3_IP_OWNPV", &p3_IP_OWNPV, &b_p3_IP_OWNPV);
   fChain->SetBranchAddress("p3_IPCHI2_OWNPV", &p3_IPCHI2_OWNPV, &b_p3_IPCHI2_OWNPV);
   fChain->SetBranchAddress("p3_ORIVX_X", &p3_ORIVX_X, &b_p3_ORIVX_X);
   fChain->SetBranchAddress("p3_ORIVX_Y", &p3_ORIVX_Y, &b_p3_ORIVX_Y);
   fChain->SetBranchAddress("p3_ORIVX_Z", &p3_ORIVX_Z, &b_p3_ORIVX_Z);
   fChain->SetBranchAddress("p3_ORIVX_XERR", &p3_ORIVX_XERR, &b_p3_ORIVX_XERR);
   fChain->SetBranchAddress("p3_ORIVX_YERR", &p3_ORIVX_YERR, &b_p3_ORIVX_YERR);
   fChain->SetBranchAddress("p3_ORIVX_ZERR", &p3_ORIVX_ZERR, &b_p3_ORIVX_ZERR);
   fChain->SetBranchAddress("p3_ORIVX_CHI2", &p3_ORIVX_CHI2, &b_p3_ORIVX_CHI2);
   fChain->SetBranchAddress("p3_ORIVX_NDOF", &p3_ORIVX_NDOF, &b_p3_ORIVX_NDOF);
   fChain->SetBranchAddress("p3_ORIVX_COV_", p3_ORIVX_COV_, &b_p3_ORIVX_COV_);
   fChain->SetBranchAddress("p3_P", &p3_P, &b_p3_P);
   fChain->SetBranchAddress("p3_PT", &p3_PT, &b_p3_PT);
   fChain->SetBranchAddress("p3_PE", &p3_PE, &b_p3_PE);
   fChain->SetBranchAddress("p3_PX", &p3_PX, &b_p3_PX);
   fChain->SetBranchAddress("p3_PY", &p3_PY, &b_p3_PY);
   fChain->SetBranchAddress("p3_PZ", &p3_PZ, &b_p3_PZ);
   fChain->SetBranchAddress("p3_M", &p3_M, &b_p3_M);
   fChain->SetBranchAddress("p3_ID", &p3_ID, &b_p3_ID);
   fChain->SetBranchAddress("p3_PIDe", &p3_PIDe, &b_p3_PIDe);
   fChain->SetBranchAddress("p3_PIDmu", &p3_PIDmu, &b_p3_PIDmu);
   fChain->SetBranchAddress("p3_PIDK", &p3_PIDK, &b_p3_PIDK);
   fChain->SetBranchAddress("p3_PIDp", &p3_PIDp, &b_p3_PIDp);
   fChain->SetBranchAddress("p3_PIDd", &p3_PIDd, &b_p3_PIDd);
   fChain->SetBranchAddress("p3_ProbNNe", &p3_ProbNNe, &b_p3_ProbNNe);
   fChain->SetBranchAddress("p3_ProbNNk", &p3_ProbNNk, &b_p3_ProbNNk);
   fChain->SetBranchAddress("p3_ProbNNp", &p3_ProbNNp, &b_p3_ProbNNp);
   fChain->SetBranchAddress("p3_ProbNNpi", &p3_ProbNNpi, &b_p3_ProbNNpi);
   fChain->SetBranchAddress("p3_ProbNNmu", &p3_ProbNNmu, &b_p3_ProbNNmu);
   fChain->SetBranchAddress("p3_ProbNNd", &p3_ProbNNd, &b_p3_ProbNNd);
   fChain->SetBranchAddress("p3_ProbNNghost", &p3_ProbNNghost, &b_p3_ProbNNghost);
   fChain->SetBranchAddress("p3_hasMuon", &p3_hasMuon, &b_p3_hasMuon);
   fChain->SetBranchAddress("p3_isMuon", &p3_isMuon, &b_p3_isMuon);
   fChain->SetBranchAddress("p3_hasRich", &p3_hasRich, &b_p3_hasRich);
   fChain->SetBranchAddress("p3_UsedRichAerogel", &p3_UsedRichAerogel, &b_p3_UsedRichAerogel);
   fChain->SetBranchAddress("p3_UsedRich1Gas", &p3_UsedRich1Gas, &b_p3_UsedRich1Gas);
   fChain->SetBranchAddress("p3_UsedRich2Gas", &p3_UsedRich2Gas, &b_p3_UsedRich2Gas);
   fChain->SetBranchAddress("p3_RichAboveElThres", &p3_RichAboveElThres, &b_p3_RichAboveElThres);
   fChain->SetBranchAddress("p3_RichAboveMuThres", &p3_RichAboveMuThres, &b_p3_RichAboveMuThres);
   fChain->SetBranchAddress("p3_RichAbovePiThres", &p3_RichAbovePiThres, &b_p3_RichAbovePiThres);
   fChain->SetBranchAddress("p3_RichAboveKaThres", &p3_RichAboveKaThres, &b_p3_RichAboveKaThres);
   fChain->SetBranchAddress("p3_RichAbovePrThres", &p3_RichAbovePrThres, &b_p3_RichAbovePrThres);
   fChain->SetBranchAddress("p3_hasCalo", &p3_hasCalo, &b_p3_hasCalo);
   fChain->SetBranchAddress("p3_L0Global_Dec", &p3_L0Global_Dec, &b_p3_L0Global_Dec);
   fChain->SetBranchAddress("p3_L0Global_TIS", &p3_L0Global_TIS, &b_p3_L0Global_TIS);
   fChain->SetBranchAddress("p3_L0Global_TOS", &p3_L0Global_TOS, &b_p3_L0Global_TOS);
   fChain->SetBranchAddress("p3_Hlt1Global_Dec", &p3_Hlt1Global_Dec, &b_p3_Hlt1Global_Dec);
   fChain->SetBranchAddress("p3_Hlt1Global_TIS", &p3_Hlt1Global_TIS, &b_p3_Hlt1Global_TIS);
   fChain->SetBranchAddress("p3_Hlt1Global_TOS", &p3_Hlt1Global_TOS, &b_p3_Hlt1Global_TOS);
   fChain->SetBranchAddress("p3_Hlt1Phys_Dec", &p3_Hlt1Phys_Dec, &b_p3_Hlt1Phys_Dec);
   fChain->SetBranchAddress("p3_Hlt1Phys_TIS", &p3_Hlt1Phys_TIS, &b_p3_Hlt1Phys_TIS);
   fChain->SetBranchAddress("p3_Hlt1Phys_TOS", &p3_Hlt1Phys_TOS, &b_p3_Hlt1Phys_TOS);
   fChain->SetBranchAddress("p3_L0PhotonDecision_Dec", &p3_L0PhotonDecision_Dec, &b_p3_L0PhotonDecision_Dec);
   fChain->SetBranchAddress("p3_L0PhotonDecision_TIS", &p3_L0PhotonDecision_TIS, &b_p3_L0PhotonDecision_TIS);
   fChain->SetBranchAddress("p3_L0PhotonDecision_TOS", &p3_L0PhotonDecision_TOS, &b_p3_L0PhotonDecision_TOS);
   fChain->SetBranchAddress("p3_L0HadronDecision_Dec", &p3_L0HadronDecision_Dec, &b_p3_L0HadronDecision_Dec);
   fChain->SetBranchAddress("p3_L0HadronDecision_TIS", &p3_L0HadronDecision_TIS, &b_p3_L0HadronDecision_TIS);
   fChain->SetBranchAddress("p3_L0HadronDecision_TOS", &p3_L0HadronDecision_TOS, &b_p3_L0HadronDecision_TOS);
   fChain->SetBranchAddress("p3_L0MuonDecision_Dec", &p3_L0MuonDecision_Dec, &b_p3_L0MuonDecision_Dec);
   fChain->SetBranchAddress("p3_L0MuonDecision_TIS", &p3_L0MuonDecision_TIS, &b_p3_L0MuonDecision_TIS);
   fChain->SetBranchAddress("p3_L0MuonDecision_TOS", &p3_L0MuonDecision_TOS, &b_p3_L0MuonDecision_TOS);
   fChain->SetBranchAddress("p3_L0ElectronDecision_Dec", &p3_L0ElectronDecision_Dec, &b_p3_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("p3_L0ElectronDecision_TIS", &p3_L0ElectronDecision_TIS, &b_p3_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("p3_L0ElectronDecision_TOS", &p3_L0ElectronDecision_TOS, &b_p3_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("p3_L0DiMuonDecision_Dec", &p3_L0DiMuonDecision_Dec, &b_p3_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("p3_L0DiMuonDecision_TIS", &p3_L0DiMuonDecision_TIS, &b_p3_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("p3_L0DiMuonDecision_TOS", &p3_L0DiMuonDecision_TOS, &b_p3_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("p3_L0GlobalDecision_Dec", &p3_L0GlobalDecision_Dec, &b_p3_L0GlobalDecision_Dec);
   fChain->SetBranchAddress("p3_L0GlobalDecision_TIS", &p3_L0GlobalDecision_TIS, &b_p3_L0GlobalDecision_TIS);
   fChain->SetBranchAddress("p3_L0GlobalDecision_TOS", &p3_L0GlobalDecision_TOS, &b_p3_L0GlobalDecision_TOS);
   fChain->SetBranchAddress("p3_Hlt1TrackMVADecision_Dec", &p3_Hlt1TrackMVADecision_Dec, &b_p3_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("p3_Hlt1TrackMVADecision_TIS", &p3_Hlt1TrackMVADecision_TIS, &b_p3_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("p3_Hlt1TrackMVADecision_TOS", &p3_Hlt1TrackMVADecision_TOS, &b_p3_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("p3_Hlt1TwoTrackMVADecision_Dec", &p3_Hlt1TwoTrackMVADecision_Dec, &b_p3_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("p3_Hlt1TwoTrackMVADecision_TIS", &p3_Hlt1TwoTrackMVADecision_TIS, &b_p3_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("p3_Hlt1TwoTrackMVADecision_TOS", &p3_Hlt1TwoTrackMVADecision_TOS, &b_p3_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("p3_Hlt1GlobalDecision_Dec", &p3_Hlt1GlobalDecision_Dec, &b_p3_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("p3_Hlt1GlobalDecision_TIS", &p3_Hlt1GlobalDecision_TIS, &b_p3_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("p3_Hlt1GlobalDecision_TOS", &p3_Hlt1GlobalDecision_TOS, &b_p3_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("L0Data_DiMuon_Pt", &L0Data_DiMuon_Pt, &b_L0Data_DiMuon_Pt);
   fChain->SetBranchAddress("L0Data_DiMuonProd_Pt1Pt2", &L0Data_DiMuonProd_Pt1Pt2, &b_L0Data_DiMuonProd_Pt1Pt2);
   fChain->SetBranchAddress("L0Data_Electron_Et", &L0Data_Electron_Et, &b_L0Data_Electron_Et);
   fChain->SetBranchAddress("L0Data_GlobalPi0_Et", &L0Data_GlobalPi0_Et, &b_L0Data_GlobalPi0_Et);
   fChain->SetBranchAddress("L0Data_Hadron_Et", &L0Data_Hadron_Et, &b_L0Data_Hadron_Et);
   fChain->SetBranchAddress("L0Data_LocalPi0_Et", &L0Data_LocalPi0_Et, &b_L0Data_LocalPi0_Et);
   fChain->SetBranchAddress("L0Data_Muon1_Pt", &L0Data_Muon1_Pt, &b_L0Data_Muon1_Pt);
   fChain->SetBranchAddress("L0Data_Muon1_Sgn", &L0Data_Muon1_Sgn, &b_L0Data_Muon1_Sgn);
   fChain->SetBranchAddress("L0Data_Muon2_Pt", &L0Data_Muon2_Pt, &b_L0Data_Muon2_Pt);
   fChain->SetBranchAddress("L0Data_Muon2_Sgn", &L0Data_Muon2_Sgn, &b_L0Data_Muon2_Sgn);
   fChain->SetBranchAddress("L0Data_Muon3_Pt", &L0Data_Muon3_Pt, &b_L0Data_Muon3_Pt);
   fChain->SetBranchAddress("L0Data_Muon3_Sgn", &L0Data_Muon3_Sgn, &b_L0Data_Muon3_Sgn);
   fChain->SetBranchAddress("L0Data_PUHits_Mult", &L0Data_PUHits_Mult, &b_L0Data_PUHits_Mult);
   fChain->SetBranchAddress("L0Data_PUPeak1_Cont", &L0Data_PUPeak1_Cont, &b_L0Data_PUPeak1_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak1_Pos", &L0Data_PUPeak1_Pos, &b_L0Data_PUPeak1_Pos);
   fChain->SetBranchAddress("L0Data_PUPeak2_Cont", &L0Data_PUPeak2_Cont, &b_L0Data_PUPeak2_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak2_Pos", &L0Data_PUPeak2_Pos, &b_L0Data_PUPeak2_Pos);
   fChain->SetBranchAddress("L0Data_Photon_Et", &L0Data_Photon_Et, &b_L0Data_Photon_Et);
   fChain->SetBranchAddress("L0Data_Spd_Mult", &L0Data_Spd_Mult, &b_L0Data_Spd_Mult);
   fChain->SetBranchAddress("L0Data_Sum_Et", &L0Data_Sum_Et, &b_L0Data_Sum_Et);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next1", &L0Data_Sum_Et_Next1, &b_L0Data_Sum_Et_Next1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next2", &L0Data_Sum_Et_Next2, &b_L0Data_Sum_Et_Next2);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev1", &L0Data_Sum_Et_Prev1, &b_L0Data_Sum_Et_Prev1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev2", &L0Data_Sum_Et_Prev2, &b_L0Data_Sum_Et_Prev2);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   Notify();
}

Bool_t ntpmakerRun2::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpmakerRun2::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpmakerRun2::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpmakerRun2_cxx
